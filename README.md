![](unigames-express/public/images/unigames-readme-completo.jpeg)

# Componenti Gruppo
+ 808374 Brumana Mattia
+ 803192 Carubelli Andrea

# Descrizione del progetto
Sviluppo di una Web Application (denominata UniGames) per l'acquisto/noleggio di videogiochi. In particolare, si propone lo sviluppo di una Web Application sia dal punto di vista del frontend che dal punto di vista del backend. L'utente, al momento dell'iscrizione, potrà abbonarsi come utente "Basic" oppure come utente "Deluxe". Un abbonamento Basic permette solamente di noleggiare i videogiochi, mentre un abbonamento Deluxe permette di noleggiare ed acquistare i videogiochi.

Il lato backend permetterà il reale salvataggio di utenti e videogiochi, memorizzati all'interno di un database. Il lato frontend permetterà invece un facile utilizzo dell'applicazione attraverso una User Interface, anche per utenti nuovi alla Web Application.

Si prevede la possibilità per ciascun utente di recensire un videogioco (mediante un voto espresso in stelle che va da 1 a 5), e di aggiungere commenti ad ogni videogame. Ogni videogame presenterà inoltre un trailer dello stesso.

Il sito prevede la possibilità di aggiungere fondi ad un portafoglio interno, utilizzato per l'acquisto/noleggio. In tale portafoglio verranno accumulate automaticamente delle ricompense giornaliere, ossia piccoli premi (ad esempio buoni sconto o monete virtuali) ottenuti ad esempio effettuando l'accesso all'applicazione per X giorni consecutivi oppure effettuando Y acquisti/noleggi in un mese. La moneta virtuale ottenuta potrà essere utilizzata per un successivo acquisto o noleggio di un videogioco.

Ogni utente registrato avrà la possibilità di creare una propria lista dei desideri e, di conseguenza, la Web Application consiglierà all'utente dei videogames correlati alle sue preferenze. Infine verrà predisposta una Live Chat (eventualmente attraverso un chatbot) per rispondere alle domande più frequenti da parte degli utenti.

L'idea è stata recuperata dal terzo assignment di Processo e Sviluppo del Software, dove è stata sviluppata una Spring Boot Application solamente dal punto di vista del frontend (utilizzo di oggetti volatili) e senza l'implementazione di LiveChat, lista dei desideri, recensioni/commenti e premi giornalieri. Tuttavia del progetto già svolto si mantiene solo l'idea di base. La Web Application verrà quindi ridisegnata e reimplementata da zero in quanto verranno utilizzati framework e linguaggi di sviluppo differenti.

# Architettura - alto livello

Lo sviluppo della Web Application sarà rivolto alle piattaforme desktop e mobile attraverso i più comuni browser, perciò l'applicazione sarà responsive.
Essa avrà una struttura MVC (Model-View-Controller) e sarà implementata attraverso i seguenti framework:

+ User Interface: HTML + Bootstrap
+ Front-End: Node.js + Express
+ Back-End: MySQL

Il front-end sarà implementato attraverso views differenti che comunicheranno attraverso delle API con il back-end, implementato attraverso un database MySQL.
La User Interface sarà implementata attraverso l'utilizzo di Bootstrap, jQuery e Javascript. Il risultato della combinazione di tali linguaggi e librerie sarà visibile in un template .ejs.
La parte riguardante Node.js verrà implementata attraverso il framework Express, framework per applicazioni web Node.js flessibile e leggero che fornisce una serie di funzioni avanzate per le applicazioni web.

![](er_diagram.png)

# Guida all'uso
+ Clonare il progetto in locale: `git clone https://gitlab.com/mattiabrumana33/labprogettazione_brumanacarubelli_unigames.git`
+ Scaricare e installare [Node.js](https://nodejs.org/it/download/)
+ Scaricare e installare Express js: `npm install express`
+ Scaricare e installare [Xampp](https://www.apachefriends.org/it/index.html)
+ Aprire PhpMyAdmin e creare un nuovo database denominato *_unigames_db_*
+ Importare nel database appena creato il file *_unigames_db_* contenuto nella cartella *_database_*
+ Digitare `npm start` all'interno della cartella *_unigames-express_*
+ Aprire un browser e navigare in `localhost:3000`
+ Per accedere come admin inserire: `username: mattia.brumana@yahoo.it`, `password: prova`
+ Per accedere come utente deluxe inserire: `username: mario.rossi@yahoo.it`, `password: prova`

N.B.
Oltre ai pacchetti di "default" installati automaticamente durante la creazione del progetto sono stati installati anche i seguenti pacchetti:

+ Express, Path, Cors, Serve-favicon, Morgan, Cookie-parser, Body-parser, Express-session, Swagger-ui-express, Swagger-ui, Swagger-ui-dist, Swagger-client, Uuid4, Uuid, Supertest, Mysql, Moment, Ejs, Cookie