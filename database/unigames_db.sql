-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Giu 09, 2020 alle 14:29
-- Versione del server: 10.4.11-MariaDB
-- Versione PHP: 7.2.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `unigames_db`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `buyvideogame`
--

CREATE TABLE `buyvideogame` (
  `idVideogame` int(11) NOT NULL,
  `idUser` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `paymentMethod` binary(1) NOT NULL,
  `total` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `buyvideogame`
--

INSERT INTO `buyvideogame` (`idVideogame`, `idUser`, `date`, `paymentMethod`, `total`) VALUES
(1, '2a02a83f-0cdf-4833-9973-c4e345eecb3d', '2020-06-09 12:23:25', 0x30, 69.99),
(3, '2a02a83f-0cdf-4833-9973-c4e345eecb3d', '2020-06-09 12:23:25', 0x30, 29.99),
(4, '8cb68147-a457-46cc-bbe3-7d93bc17279d', '2020-06-09 12:28:35', 0x30, 59.99);

-- --------------------------------------------------------

--
-- Struttura della tabella `creditcard`
--

CREATE TABLE `creditcard` (
  `idCreditCard` varchar(16) NOT NULL,
  `creationDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `modifiedDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `cvv` int(3) NOT NULL,
  `expiredDate` date NOT NULL,
  `owner` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `creditcard`
--

INSERT INTO `creditcard` (`idCreditCard`, `creationDate`, `modifiedDate`, `cvv`, `expiredDate`, `owner`) VALUES
('1234123412341234', '2020-06-09 12:22:49', '2020-06-09 12:22:49', 789, '2022-10-01', '2e053b38-1ba4-4c14-8276-1b7e17e8b4fd'),
('4567456745674567', '2020-06-09 12:27:57', '2020-06-09 12:27:57', 123, '2024-11-01', '73127189-5fa4-441b-b00a-e7956673de13');

-- --------------------------------------------------------

--
-- Struttura della tabella `genre`
--

CREATE TABLE `genre` (
  `idGenre` int(11) NOT NULL,
  `creationDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `modifiedDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `genre`
--

INSERT INTO `genre` (`idGenre`, `creationDate`, `modifiedDate`, `type`) VALUES
(1, '2020-04-14 22:28:04', '2020-04-14 22:28:04', 'Simulatore di guida'),
(2, '2020-04-14 22:28:21', '2020-04-14 22:28:21', 'Calcio'),
(3, '2020-04-14 22:28:45', '2020-04-14 22:28:45', 'Avventura dinamica'),
(4, '2020-04-14 22:28:55', '2020-04-14 22:28:55', 'Survival Horror'),
(5, '2020-04-14 22:29:40', '2020-04-14 22:29:40', 'Stealth'),
(6, '2020-04-14 22:30:21', '2020-04-14 22:30:21', 'Sparatutto in terza persona'),
(7, '2020-04-14 22:30:27', '2020-04-14 22:30:27', 'Sandbox'),
(8, '2020-04-14 22:30:59', '2020-04-14 22:30:59', 'Sparatutto in prima persona');

-- --------------------------------------------------------

--
-- Struttura della tabella `hasgenre`
--

CREATE TABLE `hasgenre` (
  `idVideogame` int(11) NOT NULL,
  `idGenre` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `hasgenre`
--

INSERT INTO `hasgenre` (`idVideogame`, `idGenre`) VALUES
(1, 8),
(2, 2),
(3, 3),
(3, 4),
(4, 5),
(5, 3),
(5, 4),
(6, 6),
(6, 7),
(7, 1),
(8, 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `platform`
--

CREATE TABLE `platform` (
  `idPlatform` int(11) NOT NULL,
  `creationDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `modifiedDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `platform`
--

INSERT INTO `platform` (`idPlatform`, `creationDate`, `modifiedDate`, `name`) VALUES
(1, '2020-04-14 22:26:11', '2020-04-14 22:26:11', 'PS4'),
(2, '2020-04-14 22:26:24', '2020-04-14 22:26:24', 'PS3'),
(3, '2020-04-14 22:26:46', '2020-04-14 22:26:46', 'Xbox One'),
(4, '2020-04-14 22:27:07', '2020-04-14 22:27:07', 'Xbox 360'),
(5, '2020-04-14 22:27:18', '2020-04-14 22:27:18', 'Nintendo DS'),
(6, '2020-04-14 22:27:30', '2020-04-14 22:27:30', 'Nintendo Switch'),
(7, '2020-04-14 22:27:46', '2020-04-14 22:27:46', 'PC');

-- --------------------------------------------------------

--
-- Struttura della tabella `playableon`
--

CREATE TABLE `playableon` (
  `idVideogame` int(11) NOT NULL,
  `idPlatform` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `playableon`
--

INSERT INTO `playableon` (`idVideogame`, `idPlatform`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 6),
(1, 7),
(2, 1),
(2, 4),
(2, 6),
(2, 7),
(3, 1),
(3, 2),
(3, 3),
(3, 4),
(3, 6),
(3, 7),
(4, 1),
(4, 7),
(5, 1),
(6, 1),
(6, 4),
(6, 7),
(7, 1),
(7, 4),
(7, 6),
(7, 7),
(8, 1),
(8, 2),
(8, 3),
(8, 4),
(8, 5),
(8, 6),
(8, 7);

-- --------------------------------------------------------

--
-- Struttura della tabella `rentvideogame`
--

CREATE TABLE `rentvideogame` (
  `idVideogame` int(11) NOT NULL,
  `idUser` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `method` binary(1) NOT NULL,
  `total` double NOT NULL,
  `dateOfStart` date NOT NULL,
  `dateOfEnd` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `rentvideogame`
--

INSERT INTO `rentvideogame` (`idVideogame`, `idUser`, `date`, `method`, `total`, `dateOfStart`, `dateOfEnd`) VALUES
(1, '8cb68147-a457-46cc-bbe3-7d93bc17279d', '2020-06-09 12:28:19', 0x30, 11.96, '0000-00-00', '0000-00-00'),
(5, '2a02a83f-0cdf-4833-9973-c4e345eecb3d', '2020-06-09 12:24:11', 0x31, 14.950000000000001, '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Struttura della tabella `review`
--

CREATE TABLE `review` (
  `idVideogame` int(11) NOT NULL,
  `idUser` varchar(255) NOT NULL,
  `creationDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `modifiedDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `text` varchar(1000) NOT NULL,
  `stars` decimal(2,1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `review`
--

INSERT INTO `review` (`idVideogame`, `idUser`, `creationDate`, `modifiedDate`, `text`, `stars`) VALUES
(1, '2a02a83f-0cdf-4833-9973-c4e345eecb3d', '2020-05-20 20:01:43', '2020-05-20 20:01:43', 'Bruttissimo, lo sconsiglio', '1.0'),
(1, '2a02a83f-0cdf-4833-9973-c4e345eecb3d', '2020-05-20 20:06:21', '2020-05-20 20:06:21', 'Sistemate i server!!!', '2.0'),
(1, '2a02a83f-0cdf-4833-9973-c4e345eecb3d', '2020-05-24 12:34:14', '2020-05-24 12:34:14', 'L\'ho provato e lo consiglio, grafica curatissima in ogni dettaglio', '4.0'),
(1, '8cb68147-a457-46cc-bbe3-7d93bc17279d', '2020-05-20 20:04:08', '2020-05-20 20:04:08', 'Gioco bellissimo, lo consiglio. Voto pieno.', '5.0'),
(2, '2a02a83f-0cdf-4833-9973-c4e345eecb3d', '2020-05-20 20:18:39', '2020-05-20 20:18:39', 'Gioco Fantastico', '5.0'),
(2, '2a02a83f-0cdf-4833-9973-c4e345eecb3d', '2020-05-22 14:35:27', '2020-05-22 14:35:27', 'Gioco bellissimo.', '4.0'),
(2, '2a02a83f-0cdf-4833-9973-c4e345eecb3d', '2020-06-03 09:58:35', '2020-06-03 09:58:35', 'Recensione di prova', '3.0'),
(2, '8cb68147-a457-46cc-bbe3-7d93bc17279d', '2020-05-22 14:38:16', '2020-05-22 14:38:16', 'Bello', '4.0'),
(3, '2a02a83f-0cdf-4833-9973-c4e345eecb3d', '2020-06-03 09:49:03', '2020-06-03 09:49:03', 'Uno dei giochi più belli a cui abbia mai giocato. Complimenti.', '5.0');

-- --------------------------------------------------------

--
-- Struttura della tabella `subscription`
--

CREATE TABLE `subscription` (
  `idSubscription` varchar(3) NOT NULL,
  `creationDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `modifiedDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `name` varchar(255) NOT NULL,
  `price` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `subscription`
--

INSERT INTO `subscription` (`idSubscription`, `creationDate`, `modifiedDate`, `name`, `price`) VALUES
('BSC', '2020-04-14 22:12:24', '2020-04-14 22:12:24', 'Basic', 0),
('DLX', '2020-04-14 22:12:49', '2020-04-14 22:12:49', 'Deluxe', 9.99);

-- --------------------------------------------------------

--
-- Struttura della tabella `user`
--

CREATE TABLE `user` (
  `idUser` varchar(255) NOT NULL,
  `creationDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `dateOfBirth` date NOT NULL,
  `placeOfBirth` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `civicNumber` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `province` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phoneNumber` varchar(255) DEFAULT NULL,
  `admin` tinyint(1) NOT NULL,
  `idSubscription` varchar(255) NOT NULL,
  `idWallet` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `user`
--

INSERT INTO `user` (`idUser`, `creationDate`, `name`, `surname`, `dateOfBirth`, `placeOfBirth`, `address`, `civicNumber`, `city`, `province`, `email`, `password`, `phoneNumber`, `admin`, `idSubscription`, `idWallet`) VALUES
('2a02a83f-0cdf-4833-9973-c4e345eecb3d', '2020-04-14 22:15:17', 'Mario', 'Rossi', '1992-09-07', 'Bergamo', 'Via San Bernardino', '114', 'Bergamo', 'Bergamo', 'mario.rossi@yahoo.it', '149cdb8620', '', 0, 'BSC', '2e053b38-1ba4-4c14-8276-1b7e17e8b4fd'),
('8cb68147-a457-46cc-bbe3-7d93bc17279d', '2020-04-14 22:14:02', 'Mattia', 'Brumana', '1996-04-03', 'Bergamo', 'Via Arzenate', '17', 'Barzana', 'Bergamo', 'mattia.brumana@yahoo.it', '149cdb8620', '', 1, 'BSC', '73127189-5fa4-441b-b00a-e7956673de13');

-- --------------------------------------------------------

--
-- Struttura della tabella `videogame`
--

CREATE TABLE `videogame` (
  `idVideogame` int(11) NOT NULL,
  `creationDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `modifiedDate` timestamp NULL DEFAULT current_timestamp(),
  `name` varchar(255) NOT NULL,
  `releaseYear` year(4) NOT NULL,
  `softwareHouse` varchar(255) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `buyPrice` double NOT NULL,
  `rentPrice` double NOT NULL,
  `image` varchar(500) NOT NULL,
  `trailer` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `videogame`
--

INSERT INTO `videogame` (`idVideogame`, `creationDate`, `modifiedDate`, `name`, `releaseYear`, `softwareHouse`, `description`, `buyPrice`, `rentPrice`, `image`, `trailer`) VALUES
(1, '2020-03-23 16:36:09', '2020-03-23 16:36:09', 'Call of Duty: Modern Warfare', 2019, 'Infinity Ward', 'Call of Duty: Modern Warfare è un videogioco sparatutto in prima persona, sviluppato da Infinity Ward per le piattaforme PlayStation 4, Xbox One e per PC. Il videogioco è stato pubblicato il 25 ottobre 2019. È il sedicesimo capitolo della serie Call of Duty (dopo Call of Duty: Black Ops IIII) e quarto capitolo della saga Modern Warfare, dopo Call of Duty: Modern Warfare 3, Call of Duty: Modern Warfare 2 e Call of Duty 4: Modern Warfare.\r\n\r\nInfinity Ward ha annunciato che il videogioco avrebbe supportato il cross-platform e, inoltre, non vi sarebbe stato più presente il Season pass, quindi i quattro DLC che usciranno in futuro, saranno gratuiti per tutti coloro che possiedono il videogioco.\r\n\r\nIl videogioco è stato presentato in anteprima all\'E3 tra l\'11 e il 13 giugno 2019, a Los Angeles.', 69.99, 2.99, 'https://www.gamersparadise.it/wp-content/uploads/2019/09/Cod-MW-1.jpg', 'https://www.youtube.com/embed/bH1lHCirCGI'),
(2, '2020-04-14 22:18:36', '2020-04-14 22:18:36', 'FIFA 20', 2019, 'EA Canada', 'FIFA 20 è un videogioco di calcio, sviluppato da EA Sports, disponibile per PlayStation 4, Xbox one, Microsoft Windows e Nintendo Switch.', 69.99, 2.99, 'https://i1.wp.com/www.gamesplus.it/wp-content/uploads/2019/09/FIFA-20.jpg?fit=1920%2C1080&ssl=1', 'https://www.youtube.com/embed/vgQNOIhRsV4'),
(3, '2020-04-14 22:19:44', '2020-04-14 22:19:44', 'The Last of Us', 2014, 'Naughty Dog', 'The Last of Us è un videogioco horror action-adventure del 2013, sviluppato da Naughty Dog e pubblicato da Sony Interactive Entertainment per PlayStation 3 e PlayStation 4.\r\n\r\nÈ stato annunciato ufficialmente il 10 dicembre 2011 durante gli Spike Video Game Awards. Il gioco si è dimostrato il migliore durante l\'Electronic Entertainment Expo 2012, aggiudicandosi 5 premi, tra cui il premio per la miglior presentazione, per il miglior gioco per console e per il miglior gioco originale.', 29.99, 0.99, 'https://i.imgur.com/tbfyty9.jpg', 'https://www.youtube.com/embed/W01L70IGBgE'),
(4, '2020-04-14 22:20:52', '2020-04-14 22:20:52', 'Spiderman', 2018, 'Insomniac Games', 'Marvel\'s Spider-Man è un videogioco d\'avventura dinamica sviluppato da Insomniac Games distribuito da Sony Interactive Entertainment in esclusiva per PlayStation 4. Pubblicato in tutto il mondo il 7 settembre 2018. Nel gioco è presente uno degli ultimi cameo di Stan Lee.\r\n\r\nIl gioco fa parte di un\'opera multimediale che racconta una nuova storia su Peter Parker e il suo nuovo costume Advanced Suit legato al fumetto Spider-Geddon successore del Ragnoverso lo spider-man in questione proviene da Terra-216\r\n\r\nDopo il successo del videogioco viene pubblicata una mini serie a fumetti dal titolo Marvel\'s Spider-Man: City at War che riporta le vicende del gioco come storia principale.', 59.99, 1.99, 'https://external-preview.redd.it/peA_3Y_2Rmap7XKUQ82pMsE1A6fMLj-tI5A51IOmKc8.jpg?auto=webp&s=560475b45bc5fdfc55540722277f001609656aa3', 'https://www.youtube.com/embed/3R2uvJqWeVg'),
(5, '2020-04-14 22:23:55', '2020-04-14 22:23:55', 'Days Gone', 2019, 'Bend Studios', 'Deacon St. John, ex militare, motociclista e cacciatore di taglie, è costretto a vivere in un mondo post-apocalittico a causa di una pandemia che ha trasformato le persone in creature sovrannaturali privati della ragione, chiamati «furiosi».\r\n\r\nL\'epidemia, scoppiata due anni prima degli eventi del gioco, ha causato la morte di innumerevoli persone e la rapida caduta delle principali città. Durante le prime fasi del contagio, Deacon si era trovato separato dalla moglie Sarah, da lui messa su un elicottero di salvataggio della NERO (ente governativo il cui incarico era quello di aiutare i superstiti dell\'epidemia) in quanto ferita da una coltellata infertagli da una bambina infetta. Rimasto a terra per aiutare l\'amico Boozer, anche lui biker e legato a Deacon da un rapporto più che fraterno, Deacon aveva dato un suo anello alla moglie con la promessa di incontrarsi poco dopo presso un campo di rifugiati, che però Deacon aveva trovato distrutto da un attacco dei furiosi senza che vi fosse', 69.99, 2.99, 'https://i.pinimg.com/originals/c0/76/a8/c076a8d325565bf1db108e23f68b9abe.jpg', 'https://www.youtube.com/embed/FKtaOY9lMvM'),
(6, '2020-04-14 22:24:54', '2020-04-14 22:24:54', 'Fortnite', 2017, 'Epic Games', 'La modalità Battle Royale di Fortnite è ambientata su un\'isola in cui 100 giocatori lottano per la sopravvivenza. La modalità è stata lanciata ufficialmente dalla Epic Games il 26 settembre 2017, mentre il trailer è stato pubblicato su Youtube il 20 settembre. Oltre alla battle royale, il gioco presenta diverse modalità a tempo limitato (LTM), che possono anche diventare permanenti, come Rissa a squadre.', 19.99, 0.79, 'https://fscl01.fonpit.de/userfiles/7176834/image/fortnite.jpg', 'https://www.youtube.com/embed/2gUtfBmw86Y'),
(7, '2020-04-14 22:25:52', '2020-04-14 22:25:52', 'Need For Speed Heat', 2019, 'Ghost Games', 'Need for Speed: Heat è un videogioco di corse sviluppato dalla Ghost Games e pubblicato dalla Electronic Arts per Microsoft Windows, PlayStation 4 e Xbox One. È il ventiquattresimo capitolo della serie Need for Speed e commemora il suo venticinquesimo anniversario. Il gioco è stato pubblicato in tutto il mondo l\'8 novembre 2019.', 69.99, 2.99, 'https://images8.alphacoders.com/103/thumb-1920-1036012.png', 'https://www.youtube.com/embed/9ewiJJe_nYI'),
(8, '2020-05-16 17:40:00', '2020-05-16 17:40:00', 'FIFA 19', 2018, 'EA Canada', 'FIFA 19 è un videogioco di calcio sviluppato da EA Sports, pubblicato il 28 settembre 2018 per PlayStation 3 (con supporto PlayStation Move), PlayStation 4, Xbox 360, Xbox One (con Kinect), Microsoft Windows e Nintendo Switch.\r\n\r\nSi tratta del 27º titolo della celebre serie. In questa edizione i testimonial ufficiali del gioco sono De Bruyne, Dybala e Neymar.\r\n\r\nAnnunciato il 6 giugno 2018 durante la conferenza stampa al E3 2018, il gioco conta dopo un mese già 80 milioni di copie vendute. Il gioco presenta per la prima volta le competizioni UEFA per club, compresa la UEFA Champions League. Il compositore Hans Zimmer e il rapper Vince Staples hanno registrato un remix dell\'inno della UEFA Champions League specifico per il gioco.', 29.99, 0.99, 'https://images3.alphacoders.com/952/thumb-1920-952166.jpg', 'https://www.youtube.com/embed/qTz8ZhNrEDA');

-- --------------------------------------------------------

--
-- Struttura della tabella `wallet`
--

CREATE TABLE `wallet` (
  `idWallet` varchar(255) NOT NULL,
  `creationDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `modifiedDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `realBalance` double NOT NULL,
  `virtualBalance` double NOT NULL,
  `idCreditCard` varchar(16) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `wallet`
--

INSERT INTO `wallet` (`idWallet`, `creationDate`, `modifiedDate`, `realBalance`, `virtualBalance`, `idCreditCard`) VALUES
('2e053b38-1ba4-4c14-8276-1b7e17e8b4fd', '2020-04-14 22:15:17', '2020-04-14 22:15:17', 0.01999999999999602, 10.045000000000002, NULL),
('73127189-5fa4-441b-b00a-e7956673de13', '2020-04-14 22:14:02', '2020-04-14 22:14:02', 28.04999999999999, 17.9875, NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `wishlist`
--

CREATE TABLE `wishlist` (
  `idUser` varchar(255) NOT NULL,
  `idVideogame` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `wishlist`
--

INSERT INTO `wishlist` (`idUser`, `idVideogame`) VALUES
('2a02a83f-0cdf-4833-9973-c4e345eecb3d', 1),
('2a02a83f-0cdf-4833-9973-c4e345eecb3d', 2),
('2a02a83f-0cdf-4833-9973-c4e345eecb3d', 3),
('2a02a83f-0cdf-4833-9973-c4e345eecb3d', 4),
('2a02a83f-0cdf-4833-9973-c4e345eecb3d', 6);

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `buyvideogame`
--
ALTER TABLE `buyvideogame`
  ADD PRIMARY KEY (`idVideogame`,`idUser`),
  ADD KEY `idUser` (`idUser`);

--
-- Indici per le tabelle `creditcard`
--
ALTER TABLE `creditcard`
  ADD PRIMARY KEY (`idCreditCard`);

--
-- Indici per le tabelle `genre`
--
ALTER TABLE `genre`
  ADD PRIMARY KEY (`idGenre`);

--
-- Indici per le tabelle `hasgenre`
--
ALTER TABLE `hasgenre`
  ADD PRIMARY KEY (`idVideogame`,`idGenre`),
  ADD KEY `idGenre` (`idGenre`);

--
-- Indici per le tabelle `platform`
--
ALTER TABLE `platform`
  ADD PRIMARY KEY (`idPlatform`);

--
-- Indici per le tabelle `playableon`
--
ALTER TABLE `playableon`
  ADD PRIMARY KEY (`idVideogame`,`idPlatform`),
  ADD KEY `idPlatform` (`idPlatform`);

--
-- Indici per le tabelle `rentvideogame`
--
ALTER TABLE `rentvideogame`
  ADD PRIMARY KEY (`idVideogame`,`idUser`),
  ADD KEY `idUser` (`idUser`);

--
-- Indici per le tabelle `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`idVideogame`,`idUser`,`creationDate`),
  ADD KEY `idUser` (`idUser`);

--
-- Indici per le tabelle `subscription`
--
ALTER TABLE `subscription`
  ADD PRIMARY KEY (`idSubscription`);

--
-- Indici per le tabelle `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`idUser`),
  ADD KEY `idSubscription` (`idSubscription`),
  ADD KEY `idWallet` (`idWallet`);

--
-- Indici per le tabelle `videogame`
--
ALTER TABLE `videogame`
  ADD PRIMARY KEY (`idVideogame`);

--
-- Indici per le tabelle `wallet`
--
ALTER TABLE `wallet`
  ADD PRIMARY KEY (`idWallet`),
  ADD KEY `idCreditCard` (`idCreditCard`);

--
-- Indici per le tabelle `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`idUser`,`idVideogame`),
  ADD KEY `idVideogame` (`idVideogame`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `genre`
--
ALTER TABLE `genre`
  MODIFY `idGenre` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT per la tabella `platform`
--
ALTER TABLE `platform`
  MODIFY `idPlatform` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT per la tabella `videogame`
--
ALTER TABLE `videogame`
  MODIFY `idVideogame` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `buyvideogame`
--
ALTER TABLE `buyvideogame`
  ADD CONSTRAINT `buyvideogame_ibfk_1` FOREIGN KEY (`idUser`) REFERENCES `user` (`idUser`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `buyvideogame_ibfk_2` FOREIGN KEY (`idVideogame`) REFERENCES `videogame` (`idVideogame`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Limiti per la tabella `hasgenre`
--
ALTER TABLE `hasgenre`
  ADD CONSTRAINT `hasgenre_ibfk_1` FOREIGN KEY (`idVideogame`) REFERENCES `videogame` (`idVideogame`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `hasgenre_ibfk_2` FOREIGN KEY (`idGenre`) REFERENCES `genre` (`idGenre`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Limiti per la tabella `playableon`
--
ALTER TABLE `playableon`
  ADD CONSTRAINT `playableon_ibfk_1` FOREIGN KEY (`idPlatform`) REFERENCES `platform` (`idPlatform`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `playableon_ibfk_2` FOREIGN KEY (`idVideogame`) REFERENCES `videogame` (`idVideogame`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Limiti per la tabella `rentvideogame`
--
ALTER TABLE `rentvideogame`
  ADD CONSTRAINT `rentvideogame_ibfk_1` FOREIGN KEY (`idUser`) REFERENCES `user` (`idUser`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `rentvideogame_ibfk_2` FOREIGN KEY (`idVideogame`) REFERENCES `videogame` (`idVideogame`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `review`
--
ALTER TABLE `review`
  ADD CONSTRAINT `review_ibfk_1` FOREIGN KEY (`idUser`) REFERENCES `user` (`idUser`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `review_ibfk_2` FOREIGN KEY (`idVideogame`) REFERENCES `videogame` (`idVideogame`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`idSubscription`) REFERENCES `subscription` (`idSubscription`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_ibfk_2` FOREIGN KEY (`idWallet`) REFERENCES `wallet` (`idWallet`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `wallet`
--
ALTER TABLE `wallet`
  ADD CONSTRAINT `wallet_ibfk_1` FOREIGN KEY (`idCreditCard`) REFERENCES `creditcard` (`idCreditCard`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `wishlist`
--
ALTER TABLE `wishlist`
  ADD CONSTRAINT `wishlist_ibfk_1` FOREIGN KEY (`idUser`) REFERENCES `user` (`idUser`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `wishlist_ibfk_2` FOREIGN KEY (`idVideogame`) REFERENCES `videogame` (`idVideogame`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
