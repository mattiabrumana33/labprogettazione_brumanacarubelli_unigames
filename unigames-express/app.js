var express = require('express');
var path = require('path');
var cors = require("cors");
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var session = require('express-session');


var swaggerUi = require('swagger-ui-express'),
    swaggerDocument = require('./swagger.json');


// ROUTES
var routes = require('./routes/index');
var userRegistration = require('./routes/userRegistration');
var login = require('./routes/login');
var testDB = require('./routes/testDB');
var userProfile = require('./routes/userProfile');
var videogame = require('./routes/videogame');
var videogames = require('./routes/videogames');
var platforms = require('./routes/platforms');
var genres = require('./routes/genres');
var cart = require('./routes/cart');

// DataBase
var mysql = require("mysql");
var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "root",
  database: "unigames_db",
  socketPath: "/Applications/MAMP/tmp/mysql/mysql.sock"
});
con.connect(function(err){
  if(err){
    console.log('Error connecting to Db');
    return;
  }
});

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(session({
	secret: 'secret',
	resave: false,
  saveUninitialized: false,
  cookie: {
    // path: '/',
    maxAge: 600000*6
}
}));

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon-32x32.png')));

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
//app.use('/api/v1', router);

app.use(cors());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
// Make our db accessible to our router
app.use(function(req,res,next){
    req.con = con;
    next();
});





// USING ROUTES
app.use('/', routes);
app.use('/cart', cart);
app.use('/userRegistration', userRegistration);
app.use('/login', login);
app.use('/testDB', testDB);
app.use('/userProfile', userProfile);
app.use('/videogames', videogames);
app.use('/platforms', platforms);
app.use('/genres', genres);
app.use('/videogame', videogame);

var sess;
app.get('/',function(req,res){
  sess = req.session;

  sess.confirmationInserted;
  sess.closeAlert;
});

module.exports = app;