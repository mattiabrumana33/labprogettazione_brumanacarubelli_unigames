describe("Buy a videogame and add cashback", function() {
    it ("Login with test and buy videogame", function() {
        cy.visit('http://localhost:3000/login')
        cy.get('input[type="email"]').type('test@test.com')
        cy.get('input[type="password"]').type('test')
        cy.get('.btn').contains('Accedi').click()

        cy.visit('http://localhost:3000/videogame/1')

        cy.get('.btn').contains('Acquista').click()

        cy.get('.button').contains('Procedi al pagamento').click()

        cy.get('.card').contains('Saldo reale').click()

        cy.get('.btn').contains('Completa il pagamento').click()        
    })
}) 