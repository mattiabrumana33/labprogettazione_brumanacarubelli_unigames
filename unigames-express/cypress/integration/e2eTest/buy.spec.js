describe("Buy and rent a videogame", function() {
    it ("Login with test and buy and rent a videogame", function() {
        cy.visit('http://localhost:3000/login')
        cy.get('input[type="email"]').type('test@test.com')
        cy.get('input[type="password"]').type('test')
        cy.get('.btn').contains('Accedi').click()

        cy.visit('http://localhost:3000/videogame/1')

        cy.get('.btn').contains('Acquista').click()
        cy.get('.button').contains('Continua lo shopping').click()

        cy.get('.btn-rent').contains('Noleggia 29.99€').click()
        cy.get('.btn-rent').contains('Noleggia').click()

        cy.get('.tavo-calendar__day-inner').contains('27').click()
        cy.get('.tavo-calendar__day-inner').contains('29').click()

        cy.get('.btn-rent').contains('Completa noleggio').click()

        cy.get('.button').contains('Procedi al pagamento').click()

        cy.get('.card').contains('Saldo reale').click()

        cy.get('.btn').contains('Completa il pagamento').click()        
    })
}) 