describe("Add something to the shopping cart", function() {
    it ("Add videogame and add rent videogame", function() {
        cy.visit('http://localhost:3000/videogames')
        cy.get('.btn-buy').contains('Acquista').click()
        cy.visit('http://localhost:3000/videogames')
        cy.get('.btn-rent').contains('Noleggia').click()

        cy.get('.tavo-calendar__day-inner').contains('27').click()
        cy.get('.tavo-calendar__day-inner').contains('28').click()

        cy.get('.btn-rent').contains('Completa noleggio').click()
        

        cy.get('.order_total_amount').should('be.visible')
        
    })
}) 