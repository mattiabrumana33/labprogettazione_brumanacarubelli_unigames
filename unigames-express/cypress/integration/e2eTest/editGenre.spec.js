describe("Edit Genre", function() {
  it("Login as admin and edit an existing genre", function() {
    cy.visit('http://localhost:3000/login')
    cy.get('input[type="email"]').type('mattia.brumana@yahoo.it')
    cy.get('input[type="password"]').type('prova')
    cy.get('.btn').contains('Accedi').click()

    cy.visit('http://localhost:3000/genres/editGenre/4')
    cy.get('input[name="inputType"]').type('testGenre')

    cy.get('.edit-genre-content').click()
    cy.get('input[name="inputVideogames"]').check({force:true})

    cy.get('.btn').contains('Conferma').click()

    cy.get('.dropdown').click()
    cy.get('.btn-link').click()
  })
})