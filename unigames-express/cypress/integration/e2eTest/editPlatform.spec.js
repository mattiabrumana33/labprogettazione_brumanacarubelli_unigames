describe("Edit Platform", function() {
  it("Login as admin and edit an existing platform", function() {
    cy.visit('http://localhost:3000/login')
    cy.get('input[type="email"]').type('mattia.brumana@yahoo.it')
    cy.get('input[type="password"]').type('prova')
    cy.get('.btn').contains('Accedi').click()

    cy.visit('http://localhost:3000/platforms/editPlatform/6')
    cy.get('input[name="inputTitle"]').type('testPlatform')

    cy.get('.edit-platform-content').click()
    cy.get('input[name="inputVideogames"]').check({force:true})

    cy.get('.btn').contains('Conferma').click()

    cy.get('.dropdown').click()
    cy.get('.btn-link').click()
  })
})