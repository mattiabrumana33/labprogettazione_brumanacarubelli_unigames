describe("Edit Videogame", function() {
  it("Login as admin and edit an existing videogame", function() {
    cy.visit('http://localhost:3000/login')
    cy.get('input[type="email"]').type('mattia.brumana@yahoo.it')
    cy.get('input[type="password"]').type('prova')
    cy.get('.btn').contains('Accedi').click()

    cy.visit('http://localhost:3000/videogames/editVideogame/4')
    cy.get('input[name="inputTitle"]').type('testVideogame')
    cy.get('input[name="inputSoftwareHouse"]').clear()
    cy.get('input[name="inputSoftwareHouse"]').type('_testSoftwareHouse')
    cy.get('textarea[name="inputDescription"]').clear()
    cy.get('textarea[name="inputDescription"]').type('_testDescription')
    cy.get('input[name="inputBuyPrice"]').clear()
    cy.get('input[name="inputBuyPrice"]').type('59.99')
    cy.get('input[name="inputRentPrice"]').clear()
    cy.get('input[name="inputRentPrice"]').type('19.99')

    cy.get('input[name="inputPlatforms"]').check({force:true})
    cy.get('input[name="inputGenres"]').check({force:true})

    cy.get('.btn').contains('Conferma').click()

    cy.get('.dropdown').click()
    cy.get('.btn-link').click()
  })
})