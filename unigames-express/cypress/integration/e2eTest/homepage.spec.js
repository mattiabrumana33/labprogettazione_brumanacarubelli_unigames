describe("Homepage test", function() {
    it("Visualize homepage)", function() {
      cy.visit('http://localhost:3000/')
      cy.get('.navbar').should('be.visible')
      cy.get('.owl-next').should('be.visible')
      cy.get('.single-games-slide').should('be.visible')
      cy.get('.single-video-widget').should('be.visible')
      cy.get('.video-playground').should('be.visible')
      cy.get('.footer-1').should('be.visible')
    })
  })
  