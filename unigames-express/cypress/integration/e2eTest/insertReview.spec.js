describe("New Review", function() {
  it("Login as user and add a new review", function() {
    cy.visit('http://localhost:3000/login')
    cy.get('input[type="email"]').type('mario.rossi@yahoo.it')
    cy.get('input[type="password"]').type('prova')
    cy.get('.btn').contains('Accedi').click()

    cy.visit('http://localhost:3000/videogames')

    cy.get('.game-title').eq(0).click()

    cy.get('textarea[name="inputText"]').type('sto inserendo il commento')
    cy.get('label[name="r4"]').click()
    cy.get('.btn').contains('Inserisci').click()

    cy.get('.dropdown').click()
    cy.get('.btn-link').click()
  })
})
