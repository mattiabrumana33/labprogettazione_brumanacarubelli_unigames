describe("New Credit Card", function() {
    it ("Login with testand add a new credit card", function() {
        cy.visit('http://localhost:3000/login')
        cy.get('input[type="email"]').type('test@test.com')
        cy.get('input[type="password"]').type('test')
        cy.get('.btn').contains('Accedi').click()

        cy.visit('http://localhost:3000/userProfile/wallet')
        cy.get('a[href*="/userProfile/creditCard"]').click({force:true})

        cy.get('input[name="creditcard"]').type('4023600473786944')
        cy.get('input[name="ccexp"]').type('1022')
        cy.get('input[name="cccvc"]').type('123')
        cy.get('input[name="placeholder"]').type('Test Test')

        cy.get('.btn').contains('Aggiungi carta').click()

        
    })
}) 