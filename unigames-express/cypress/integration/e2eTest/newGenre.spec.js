describe("New Genre", function() {
  it("Login as admin and add a new genre", function() {
    cy.visit('http://localhost:3000/login')
    cy.get('input[type="email"]').type('mattia.brumana@yahoo.it')
    cy.get('input[type="password"]').type('prova')
    cy.get('.btn').contains('Accedi').click()

    cy.visit('http://localhost:3000/genres/newGenre')
    cy.get('input[name="inputType"]').type('testGenre')

    cy.get('input[name="inputVideogames"]').check({force:true})

    cy.get('.btn').contains('Conferma').click()

    cy.get('.dropdown').click()
    cy.get('.btn-link').click()
  })
})
