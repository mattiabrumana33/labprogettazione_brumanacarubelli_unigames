describe("New Videogame", function() {
  it("Login as admin and add a new videogame", function() {
    cy.visit('http://localhost:3000/login')
    cy.get('input[type="email"]').type('mattia.brumana@yahoo.it')
    cy.get('input[type="password"]').type('prova')
    cy.get('.btn').contains('Accedi').click()

    cy.visit('http://localhost:3000/videogames/newVideogame')
    cy.get('input[name="inputTitle"]').type('testVideogame')
    cy.get('input[name="inputSoftwareHouse"]').type('testSoftwareHouse')
    cy.get('textarea[name="inputDescription"]').type('testDescription')
    cy.get('input[name="inputBuyPrice"]').type('69.99')
    cy.get('input[name="inputRentPrice"]').type('29.99')

    cy.get('input[name="inputPlatforms"]').check({force:true})
    cy.get('input[name="inputGenres"]').check({force:true})

    cy.get('.btn').contains('Conferma').click()

    cy.get('.dropdown').click()
    cy.get('.btn-link').click()
  })
})
