describe("Recharge wallet", function() {
    it ("Login with test and recharge my wallet", function() {
        //login
        cy.visit('http://localhost:3000/login')
        cy.get('input[type="email"]').type('test@test.com')
        cy.get('input[type="password"]').type('test')
        cy.get('.btn').contains('Accedi').click()

        //visiting page wallet and click "ricarica saldo"
        cy.visit('http://localhost:3000/userProfile/wallet')
        cy.get('.btn').contains('Ricarica saldo').click()

        //visiting page to add money to my wallet
        cy.get('a[href*="/userProfile/rechargeWalletRecharge"]').click({force:true})

        //adding 50 to my wallet
        cy.get('input[name="Amount"]').clear()
        cy.get('input[name="Amount"]').type('50')

        //recharge my wallet
        cy.get('.btn').contains('Ricarica').click()

        
    })
}) 