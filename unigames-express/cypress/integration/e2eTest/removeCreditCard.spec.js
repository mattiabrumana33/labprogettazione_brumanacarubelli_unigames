describe("Remove Credit Card", function() {
    it ("Login with test and remove a credit card", function() {
        cy.visit('http://localhost:3000/login')
        cy.get('input[type="email"]').type('test@test.com')
        cy.get('input[type="password"]').type('test')
        cy.get('.btn').contains('Accedi').click()

        cy.visit('http://localhost:3000/userProfile/wallet')
        cy.visit('http://localhost:3000/userProfile/removeCreditCard/4023600473786945')

        cy.get('.btn').contains('Si, rimuovi').click()

        
    })
}) 