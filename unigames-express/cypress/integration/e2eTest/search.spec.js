describe("Search Videogames", function() {
  it("Search videogames with one selected filter (such as genre)", function() {
    cy.visit('http://localhost:3000/videogames')
    cy.get('input[name="inputSearch"]').type('sparatutto')
    cy.get('select[name="inputSearchParameter"]').select('Genere')

    cy.get('button[name="searchButton"]').click()
  })
})
