describe("Update user email", function() {
    it ("Should update email profile", function() {
        cy.visit('http://localhost:3000/login')
        cy.get('input[type="email"]').type('test@test.com')
        cy.get('input[type="password"]').type('test')
        cy.get('.btn').contains('Accedi').click()


        cy.visit('http://localhost:3000/userProfile/updateEmail')
        cy.get('input[name="inputEmail"]').type('test@test.com')
        cy.get('input[name="inputEmailConfirm"]').type('test@test.com')

        cy.get('.btn').contains('Aggiorna email').click()
    

    })
}) 