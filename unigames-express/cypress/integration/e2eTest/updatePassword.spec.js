describe("Update user password", function() {
    it ("Should update password profile", function() {
        cy.visit('http://localhost:3000/login')
        cy.get('input[type="email"]').type('test@test.com')
        cy.get('input[type="password"]').type('test')
        cy.get('.btn').contains('Accedi').click()


        cy.visit('http://localhost:3000/userProfile/updatePassword')
        cy.get('input[name="inputPassword1"]').type('test')
        cy.get('input[name="inputPassword2"]').type('test')

        cy.get('.btn').contains('Aggiorna password').click()
    

    })
}) 