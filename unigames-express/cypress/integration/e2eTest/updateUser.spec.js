describe("Update user profile name and surname", function() {
    it ("Should be updated", function() {

        cy.visit('http://localhost:3000/login')
        cy.get('input[type="email"]').type('test@test.com')
        cy.get('input[type="password"]').type('test')
        cy.get('.btn').contains('Accedi').click()

        cy.visit('http://localhost:3000/userProfile/updateUser')
        cy.get('input[name="inputName"]').type('testUpdated')
        cy.get('input[name="inputSurname"]').type('testUpdated')

        cy.get('.btn').contains('Aggiorna il tuo profilo').click()
    })
}) 