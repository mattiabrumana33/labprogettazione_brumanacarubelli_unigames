describe("User Registration", function() {
    it ("Register a user", function() {
        cy.visit('http://localhost:3000/userRegistration')
        cy.get('input[name="inputName"]').type('test')
        cy.get('input[name="inputSurname"]').type('test')
        cy.get('input[name="inputDateOfBirth"]').type('1996-11-18')
        cy.get('input[name="inputPlaceOfBirth"]').type('test')
        cy.get('input[name="inputAddress"]').type('test')
        cy.get('input[name="inputCivicNumber"]').type('test')
        cy.get('input[name="inputProvince"]').type('test')
        cy.get('input[name="inputCity"]').type('test')
        cy.get('input[name="inputEmail"]').type('test@test.com')
        cy.get('input[name="inputEmailConfirm"]').type('test@test.com')
        cy.get('input[name="inputPassword1"]').type('test')
        cy.get('input[name="inputPassword2"]').type('test')
        cy.get('input[name="inputPhoneNumber"]').type('test')

        cy.get('input[name="checkSubscription"]').check({force: true})


        cy.get('.btn').contains('Registrati').click()
    })
}) 