describe("Add a videogame from personal wishlist", function() {
  it("Login as member and add a new videogame to wishlist", function() {
    cy.visit('http://localhost:3000')
    cy.visit('http://localhost:3000/login')
    cy.get('input[type="email"]').type('mario.rossi@yahoo.it')
    cy.get('input[type="password"]').type('prova')
    cy.get('.btn').contains('Accedi').click()

    cy.wait(1000)

    cy.visit('http://localhost:3000/videogames')
    cy.get('a[name="add_to_wishlist"]').eq(6).click()

    cy.get('span[name="closeAlert"]').click()

    cy.visit('http://localhost:3000/userProfile/wishlist')

    cy.wait(1000)

    cy.get('.dropdown').click()
    cy.get('.btn-link').click()
  })
})
