var express = require('express');
var router = express.Router();
var moment = require('moment');
const nodemailer = require('nodemailer');

let transport = nodemailer.createTransport({
    host: 'smtp.mailtrap.io',
    port: 2525,
    auth: {
       user: 'd1cf58054e3c16',
       pass: 'aa9e976578f6cf'
    }
});
var videogames = [];
var rentArray = [];
var daysOfRent = [];
var startDay = [];
var endDay = [];

router.get('/', function(req, res, next) {

    res.render('cart');
});

router.post('/add/:id', function(req, res, next) {
    var db = req.con;
    var idVideogame = req.params.id;
    var rent = 0;
    var daysRent = 0;
    startDay.push('0');
    endDay.push('0');
    db.query('SELECT * FROM videogame WHERE idVideogame = ?', [idVideogame], function(err,results){

        videogame = req.session;
        isRent = req.session;
        days = req.session;

        videogames.push(results[0]);
        daysOfRent.push(daysRent);  
        rentArray.push(rent);

        req.session.daysRent = daysOfRent;
        req.session.rent = rentArray;
        req.session.vid = videogames;

        console.log(req.session.vid);
        console.log(req.session.rent);
        res.redirect('/cart');
    });
});

router.post('/addRent/:id', function(req, res, next) {
    var db = req.con;
    var idVideogame = req.params.id;
    var rent = 1;
    var daysRent = parseInt(req.body.rangeInputHidden);
    var daysRent = daysRent + 1;

    startDay.push(req.body.startDay);
    endDay.push(req.body.endDay);
    
    console.log(daysRent);
    db.query('SELECT * FROM videogame WHERE idVideogame = ?', [idVideogame], function(err,results){
        videogame = req.session;
        isRent = req.session;
        days = req.session;
        
        videogames.push(results[0]);
        rentArray.push(rent);
        daysOfRent.push(daysRent);

        req.session.daysRent = daysOfRent;
        req.session.rent = rentArray;
        req.session.vid = videogames;

        console.log(req.session.vid);
        console.log(rentArray);

        res.redirect('/cart');
    });
});

router.get('/remove/:id', function(req, res, next) {

    var idVideogame = req.params.id;
    console.log(videogames.length);
    for( var i = 0; i < videogames.length; i++){ 
        if ( videogames[i].idVideogame == idVideogame) { 
            //console.log(videogames[0][i].idVideogame);
            //console.log(videogames[0][i]);
            videogames.splice(i, 1); 
            rentArray.splice(i,1);
            daysOfRent.splice(i,1);
            startDay.splice(i,1);
            endDay.splice(i,1);
        }
    }
        videogame = req.session;
        isRent = req.session;
        days = req.session;
        req.session.daysRent = daysOfRent;
        req.session.vid = videogames;
        req.session.rent = rentArray;
        console.log(req.session.vid);
        
        res.redirect('/cart');
});

router.get('/buy', function(req, res, next) {
    var db = req.con;
    if (req.session.loggedin == true){
        var idWallet =  req.session.user[0].idWallet;
        var realBalance;
        var virtualBalance;
        db.query('SELECT realBalance, virtualBalance FROM Wallet WHERE idWallet = ?',[idWallet], function(err,result){
            realBalance = result[0].realBalance;
            virtualBalance = result[0].virtualBalance;

            res.render('buy', {realBalance:realBalance, virtualBalance:virtualBalance});
        });
    }
    else{
        res.render('buy');
    }
    
});

router.post('/buy', function(req, res, next) {
    var db = req.con;
    var idUser = req.session.user[0].idUser;
    var idWallet = req.session.user[0].idWallet;
    var totalPrice = parseFloat(req.body.totalPrice);
    var buyed = 0;
    console.log("TOTAL PRICE");
    console.log(totalPrice);
    if (req.body.checkPayment == 'real'){
        var paymentMethod = 0;
	}else {
		var paymentMethod = 1;
    }
    console.log("PAYMENT METHOD");
    console.log(paymentMethod);
    for(var i = 0; i < videogames.length; i++){
        if (rentArray[i] == 0){
            db.query('INSERT INTO buyvideogame (idVideogame, idUser, paymentMethod, total) VALUES("'+videogames[i].idVideogame+'","'+idUser+'","'+paymentMethod+'","'+videogames[i].buyPrice+'")', function(err,rows){
                if(err) throw err;
            });
        }
        else if (rentArray[i] == 1){
            db.query('INSERT INTO rentVideogame (idVideogame, idUser, method, total, dateOfStart, dateOfEnd) VALUES("'+videogames[i].idVideogame+'","'+idUser+'","'+paymentMethod+'","'+(videogames[i].rentPrice)*daysOfRent[i]+'","'+moment(startDay[i]).format("DD-MM-YYYY")+'","'+moment(endDay[i]).format("DD-MM-YYYY")+'")', function(err,rows){
                if(err) throw err;
            });
        }
    }
    if(paymentMethod == 0){
        db.query('SELECT realBalance FROM Wallet WHERE idWallet = ?',[idWallet], function(err,result){
            realBalance = result[0].realBalance;
            realBalance = realBalance - totalPrice;
            
            db.query('UPDATE Wallet SET realBalance = ? WHERE idWallet = ? ',[realBalance, idWallet], function(err,rows){
                if(err) throw err;
            });

        });
        db.query('SELECT virtualBalance FROM Wallet WHERE idWallet = ?',[idWallet], function(err,result){
            var cashback  = 0.25*totalPrice;
            virtualBalance = result[0].virtualBalance;
            virtualBalance = virtualBalance + cashback;
            db.query('UPDATE Wallet SET virtualBalance = ? WHERE idWallet = ? ',[virtualBalance, idWallet], function(err,rows){
                if(err) throw err;
            });
        });

    }
    else{
        db.query('SELECT virtualBalance FROM Wallet WHERE idWallet = ?',[idWallet], function(err,result){
            virtualBalance = result[0].virtualBalance;
            virtualBalance = virtualBalance - totalPrice;
            db.query('UPDATE Wallet SET virtualBalance = ? WHERE idWallet = ? ',[virtualBalance, idWallet], function(err,rows){
                if(err) throw err;
            });
        });

    }
    if (i == videogames.length){
        buyed = 1;
    }
    console.log(buyed);
    if (buyed == 1){
        const message = {
            from: 'unigames@unigames.com', // Sender address
            to: req.session.user[0].email,         // List of recipients
            subject: 'Acquisto avvenuto con successo', // Subject line
            text: 'Questa email per confermarti che l\'acquisto è avvenuto con successo' // Plain text body
        };
        transport.sendMail(message, function(err, info) {
            console.log("INVIO EMAIL");
            console.log(buyed);
            if (err) {
              console.log(err)
            } else {
              console.log(info);
            }
        });
    }
    res.redirect('/userProfile/wallet');
});


module.exports = router;