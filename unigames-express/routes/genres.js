var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
    sess=req.session;
	var db = req.con;
	db.query('SELECT * FROM Genre',function(err,rows){
        if(err) throw err;

        if (typeof sess.confirmationInserted == 'undefined')
            p = "";
        else
            p = sess.confirmationInserted;

        if (typeof sess.closeAlert == 'undefined')
            c = "";
        else
            c = sess.closeAlert;

        res.render('genres', {data:rows, confirmationInserted:p, closeAlert:c});
	});
});

router.get('/newGenre', function (req, res, next) {
	sess=req.session;
	var db = req.con;
	db.query('SELECT * FROM Videogame', function(err,rows){
		if(err) throw err;

		if (typeof sess.confirmationInserted == 'undefined')
            p = "";
        else
            p = sess.confirmationInserted;

        if (typeof sess.closeAlert == 'undefined')
            c = "";
        else
			c = sess.closeAlert;

		res.render('newGenre', {data:rows, confirmationInserted:p, closeAlert:c});
	});
});

router.get('/editGenre/:id', function(req,res,next){
	sess=req.session;
	var db = req.con;
	var idGenre = req.params.id;
	db.query('SELECT * FROM Genre WHERE idGenre=?',[idGenre], function(err,genre){
		if(err) throw err;
		db.query('SELECT * FROM Videogame', function(err,all_videogames){
			if(err) throw err;
			db.query('SELECT idVideogame FROM HasGenre WHERE idGenre=?',[idGenre],function(err,videogames_selected){
				if(err) throw err;

				if (typeof sess.confirmationInserted == 'undefined')
					p = "";
				else
					p = sess.confirmationInserted;

				if (typeof sess.closeAlert == 'undefined')
					c = "";
				else
					c = sess.closeAlert;

				res.render('editGenre', {gn:genre,all_vg:all_videogames, sel_vg:videogames_selected, confirmationInserted:p, closeAlert:c});
			});
		});
	});
});

router.get('/removeGenre/:id', function(req,res,next){
	sess=req.session;
	var db=req.con;
	var idGenre = req.params.id;
	db.query('DELETE FROM HasGenre WHERE idGenre=?',[idGenre],function(err,rows){
		if(err) throw err;
	});

	db.query('DELETE FROM Genre WHERE idGenre=?',[idGenre],function(err,rows){
		if(err) throw err;
	});

	req.session.confirmationInserted = "Il genere è stato rimosso con successo.";
    req.session.closeAlert = "X";
    res.redirect('/genres');
});

router.post('/newGenre/checkAvailability', function (req, res, next) {
	var db = req.con;
	var type = req.body.type;
	db.query('SELECT * FROM Genre WHERE type = ?',[type], function(err,results,rows){
		if(err) throw err;
		res.send(results);
		res.end();
	});
});

router.post('/editGenre/checkAvailability', function (req, res, next) {
	var db = req.con;
	var id = req.body.id;
	var type = req.body.type;
	db.query('SELECT * FROM Genre WHERE type = ?',[type], function(err,results,rows){
		if(err) throw err;

		if(results.length > 0){
			var fields = JSON.parse(JSON.stringify(results[0]));
			var idGenre = fields.idGenre;

			if(id != idGenre){
				results = [];
			}
		}

		res.send(results);
		res.end();
	});
});

router.post('/deleteVariables', function(res,req,next){
    delete sess.confirmationInserted;
    delete sess.closeAlert;
    sess.save();
    req.end();
});

router.post('/newGenre', function(req, res, next) {
	var db = req.con;
	var type = req.body.inputType;
	var videogames = req.body.inputVideogames;

	db.query('INSERT INTO Genre (type) VALUES ("'+type+'")', function(err,rows){
        if(err) throw err;
	});

	db.query('SELECT idGenre FROM Genre WHERE type = ?',[type], function(err,results,rows){
		if(err) throw err;
		var fields = JSON.parse(JSON.stringify(results[0]));
		var idGenre = fields.idGenre;

		if (videogames != null){
			for(var i=0;i<videogames.length;i++){
				db.query('INSERT INTO HasGenre (idVideogame, idGenre) VALUES ('+videogames[i]+', '+idGenre+')', function(err,rows){
					if(err) throw err;
				});
			}
		}
	});

    req.session.confirmationInserted = "Il genere è stato inserito con successo.";
    req.session.closeAlert = "X";
    res.redirect('/genres');
    res.end();
});

router.post('/editGenre', function(req, res, next) {
	var db = req.con;
	var idGenre = req.body.inputId;
	var type = req.body.inputType;
	var videogames = req.body.inputVideogames;

	db.query('UPDATE Genre SET type=? WHERE idGenre=?',[type,idGenre], function(err,rows){
        if(err) throw err;
	});

	db.query('DELETE FROM HasGenre WHERE idGenre=?',[idGenre], function(err,rows){
		if(err) throw err;
	});

	if (videogames != null){
		for(var i=0;i<videogames.length;i++){
			db.query('INSERT INTO HasGenre (idVideogame, idGenre) VALUES ('+videogames[i]+', '+idGenre+')', function(err,rows){
				if(err) throw err;
			});
		}
	}

    req.session.confirmationInserted = "Il genere è stato modificato con successo.";
    req.session.closeAlert = "X";
    res.redirect('/genres');
    res.end();
});

module.exports = router;