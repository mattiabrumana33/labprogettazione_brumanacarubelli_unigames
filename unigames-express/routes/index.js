var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  sess = req.session;
  videogame = req.session;
  isRent = req.session;
  days = req.session;

  var db = req.con;
  db.query('SELECT * FROM Videogame LIMIT 8', function(err,videogames){
    if(err) throw err;
    db.query('SELECT * FROM Genre',function(err,genres){
			if(err) throw err;

			db.query('SELECT * FROM HasGenre', function(err,hasgenre){
				if(err) throw err;

				db.query('SELECT * FROM Review', function(err,rev){
					if(err) throw err;
          res.render('index', { title: 'Unigames', sess: sess, videogames:videogames, gn:genres, hasg:hasgenre, review:rev});
        });
      });
    });
  });
});

module.exports = router;
