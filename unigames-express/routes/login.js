var express = require('express');
var router = express.Router();

var crypto = require('crypto'),
algorithm = 'aes-256-ctr',
password = 'd6F3Efeq';

function encrypt(text){
	var cipher = crypto.createCipher(algorithm,password)
	var crypted = cipher.update(text,'utf8','hex')
	crypted += cipher.final('hex');
	return crypted;
  }

router.get('/', function (req, res, next) {
	sess = req.session;
	res.render('login');
});

router.post('/auth', function(request, response) {
    var db = request.con;
	var email = request.body.email;
	var password = encrypt(request.body.password);
	if (email && password) {
		db.query('SELECT * FROM User WHERE email = ? AND password = ?', [email, password], function(error, results, fields) {
			if (results.length > 0) {
				sess = request.session;
				request.session.loggedin = true;
				request.session.email = email;
				request.session.user = results;
				response.redirect('/');
			}
			response.end();
		});
	} else {
		response.end();
	}
});

router.post('/logout', function (req, res, next) {
	req.session.loggedin = false;
	delete req.session.email;
	delete req.session.user;
    sess.save();
	res.redirect('/');
});

router.post('/check', function (req, res, next){
	var db = req.con;
	var email = req.body.email;
	var password = req.body.password;
	var password = encrypt(req.body.password);
	if (email && password){
		db.query('SELECT * FROM User WHERE email = ? AND password = ?', [email, password], function(err, results, fields) {
			if(err) throw err;
			res.send(results);
		});
	}
});


module.exports = router;