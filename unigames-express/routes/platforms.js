var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
    sess=req.session;
	var db = req.con;
	db.query('SELECT * FROM Platform',function(err,rows){
        if(err) throw err;

        if (typeof sess.confirmationInserted == 'undefined')
            p = "";
        else
            p = sess.confirmationInserted;

        if (typeof sess.closeAlert == 'undefined')
            c = "";
        else
            c = sess.closeAlert;

        res.render('platforms', {data:rows, confirmationInserted:p, closeAlert:c});
	});
});

router.get('/newPlatform', function (req, res, next) {
	sess=req.session;
	var db = req.con;
	db.query('SELECT * FROM Videogame', function(err,rows){
		if(err) throw err;

		if (typeof sess.confirmationInserted == 'undefined')
            p = "";
        else
            p = sess.confirmationInserted;

        if (typeof sess.closeAlert == 'undefined')
            c = "";
        else
			c = sess.closeAlert;

		res.render('newPlatform', {data:rows, confirmationInserted:p, closeAlert:c});
	});
});

router.get('/editPlatform/:id', function(req,res,next){
	sess=req.session;
	var db = req.con;
	var idPlatform = req.params.id;
	db.query('SELECT * FROM Platform WHERE idPlatform=?',[idPlatform], function(err,platform){
		if(err) throw err;
		db.query('SELECT * FROM Videogame', function(err,all_videogames){
			if(err) throw err;
			db.query('SELECT idVideogame FROM PlayableOn WHERE idPlatform=?',[idPlatform],function(err,videogames_selected){
				if(err) throw err;

				if (typeof sess.confirmationInserted == 'undefined')
					p = "";
				else
					p = sess.confirmationInserted;

				if (typeof sess.closeAlert == 'undefined')
					c = "";
				else
					c = sess.closeAlert;
				res.render('editPlatform', {pl:platform,all_vg:all_videogames, sel_vg:videogames_selected, confirmationInserted:p, closeAlert:c});
			});
		});
	});
});

router.get('/removePlatform/:id', function(req,res,next){
	sess=req.session;
	var db=req.con;
	var idPlatform = req.params.id;
	db.query('DELETE FROM PlayableOn WHERE idPlatform=?',[idPlatform],function(err,rows){
		if(err) throw err;
	});

	db.query('DELETE FROM Platform WHERE idPlatform=?',[idPlatform],function(err,rows){
		if(err) throw err;
	});

	req.session.confirmationInserted = "La piattaforma è stata rimossa con successo.";
    req.session.closeAlert = "X";
    res.redirect('/platforms');
});

router.post('/newPlatform/checkAvailability', function (req, res, next) {
	var db = req.con;
	var name = req.body.name;
	db.query('SELECT * FROM Platform WHERE name = ?',[name], function(err,results,rows){
		if(err) throw err;
		res.send(results);
		res.end();
	});
});

router.post('/editPlatform/checkAvailability', function (req, res, next) {
	var db = req.con;
	var id = req.body.id;
	var name = req.body.name;
	db.query('SELECT * FROM Platform WHERE name = ?',[name], function(err,results,rows){
		if(err) throw err;

		if(results.length > 0){
			var fields = JSON.parse(JSON.stringify(results[0]));
			var idPlatform = fields.idPlatform;

			if(id != idPlatform){
				results = [];
			}
		}

		res.send(results);
		res.end();
	});
});

router.post('/deleteVariables', function(res,req,next){
    delete sess.confirmationInserted;
    delete sess.closeAlert;
    sess.save();
    req.end();
});

router.post('/newPlatform', function(req, res, next) {
	var db = req.con;
	var name = req.body.inputTitle;
	var videogames = req.body.inputVideogames;

	db.query('INSERT INTO Platform (name) VALUES ("'+name+'")', function(err,rows){
        if(err) throw err;
	});

	db.query('SELECT idPlatform FROM Platform WHERE name = ?',[name], function(err,results,rows){
		if(err) throw err;
		var fields = JSON.parse(JSON.stringify(results[0]));
		var idPlatform = fields.idPlatform;

		if (videogames != null){
			for(var i=0;i<videogames.length;i++){
				db.query('INSERT INTO PlayableOn (idVideogame, idPlatform) VALUES ('+videogames[i]+', '+idPlatform+')', function(err,rows){
					if(err) throw err;
				});
			}
		}
	});

    req.session.confirmationInserted = "La piattaforma è stata inserita con successo.";
    req.session.closeAlert = "X";
    res.redirect('/platforms');
    res.end();
});

router.post('/editPlatform', function(req, res, next) {
	var db = req.con;
	var idPlatform = req.body.inputId;
	var name = req.body.inputTitle;
	var videogames = req.body.inputVideogames;

	db.query('UPDATE Platform SET name=? WHERE idPlatform=?',[name,idPlatform], function(err,rows){
        if(err) throw err;
	});

	db.query('DELETE FROM PlayableOn WHERE idPlatform=?',[idPlatform], function(err,rows){
		if(err) throw err;
	});

	if (videogames != null){
		for(var i=0;i<videogames.length;i++){
			db.query('INSERT INTO PlayableOn (idVideogame, idPlatform) VALUES ('+videogames[i]+', '+idPlatform+')', function(err,rows){
				if(err) throw err;
			});
		}
	}

    req.session.confirmationInserted = "La piattaforma è stata modificata con successo.";
    req.session.closeAlert = "X";
    res.redirect('/platforms');
    res.end();
});

module.exports = router;