var express = require('express');
var router = express.Router();
var uuid4 = require('uuid4');

var crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'd6F3Efeq';

function encrypt(text){
  var cipher = crypto.createCipher(algorithm,password)
  var crypted = cipher.update(text,'utf8','hex')
  crypted += cipher.final('hex');
  return crypted;
}

// TEST CONNECTION TO DATABASE
router.post('/connection', function(req, res, next) {
    var db = req.con;
    res.end();
});

//CRUD OPERATION USER

router.post('/insertUser', function(req, res, next) {
	var db = req.con;
	var idUser = uuid4();
	var idWallet = uuid4();
	var idSubscription = "";
	if (req.body.checkSubscription == 'deluxe'){
		var idSubscription = "DLX";
	}else{
		var idSubscription = "BSC";
	}

	var name = req.body.inputName;
	var surname = req.body.inputSurname;
	var dateOfBirth = req.body.inputDateOfBirth;
	var placeOfBirth = req.body.inputPlaceOfBirth;
	var address = req.body.inputAddress;
	var civicNumber = req.body.inputCivicNumber;
	var city = req.body.inputCity;
	var province = req.body.inputProvince;
	var email = req.body.inputEmail;
	var password = encrypt(req.body.inputPassword1);
	var phoneNumber = req.body.inputPhoneNumber;

	var realBalance = 0;
	var virtualBalance = 0;

	//QUERY INSERT WALLET
	db.query('INSERT INTO Wallet (idWallet, realBalance, virtualBalance) VALUES ("'+idWallet+'","'+realBalance+'","'+virtualBalance+'")', function(err,rows){
		if(err) throw err;
		// console.log("Portafoglio aggiunto con successo");
	});
	//QUERY INSERT USER
	db.query('INSERT INTO User (idUser, name, surname, dateOfBirth, placeOfBirth, address, civicNumber, city, province, email, password, phoneNumber, admin, idSubscription, idWallet) VALUES ("'+idUser+'","'+name+'", "'+surname+'", "'+dateOfBirth+'", "'+placeOfBirth+'", "'+address+'", "'+civicNumber+'", "'+city+'", "'+province+'", "'+email+'", "'+password+'", "'+phoneNumber+'", 0, "'+idSubscription+'", "'+idWallet+'")', function(err,rows){
		if(err) throw err;
		//res.render('confirmRegistration', {name: name});
		// console.log("Utente registrato con successo");
    });
    //QUERY SEARCH USER THAT WAS JUST ADDED
    db.query('SELECT * FROM User WHERE name = ?',[name], function(err,rows){
		if(err) throw err;
		//res.render('confirmRegistration', {name: name});
		// console.log("Utente registrato con successo");
		res.end();
    });
});

router.post('/deleteUser', function(req, res, next) {
	var db = req.con;

    var name = req.body.inputName;

    db.query('DELETE FROM User WHERE name = ? ',[name], function(err,rows){
        if(err) throw err;
        res.end();
    });
});

router.post('/updateUser', function(req, res, next) {
	var db = req.con;

    var name = req.body.inputName;
    //var nameUpdate = req.body.updateName;

    db.query('UPDATE User SET name = "AndreaTestUpdate" WHERE name = ? ',[name], function(err,rows){
        if(err) throw err;
        res.end();
    });
});

//CRUD OPERATION VIDEOGAME

router.post('/insertVideogame', function(req, res, next) {
	var db = req.con;

	var name = req.body.inputName;
	var releaseYear = req.body.inputReleaseYear;
	var softwareHouse = req.body.inputSoftwareHouse;
	var description = req.body.inputDescription;
	var buyPrice = req.body.inputBuyPrice;
	var rentPrice = req.body.inputRentPrice;
	var image = req.body.inputImage;
	var trailer = req.body.inputTrailer;
    
	//QUERY INSERT VIDEOGAME
	db.query('INSERT INTO Videogame (name, releaseYear, softwareHouse, description ,buyPrice, rentPrice) VALUES ("'+name+'","'+releaseYear+'","'+softwareHouse+'", "'+description+'", "'+buyPrice+'", "'+rentPrice+'")', function(err,rows){
		if(err) throw err;
	});
    //QUERY SEARCH VIDEOGAME THAT WAS JUST ADDED
    db.query('SELECT * FROM Videogame WHERE name = "VideogameTest"',[name], function(err,rows){
		if(err) throw err;
		res.end();
    });
});

router.post('/deleteVideogame', function(req, res, next) {
	var db = req.con;
    db.query('DELETE FROM Videogame WHERE name = "VideogameTest" ', function(err,rows){
        if(err) throw err;
        res.end();
    });
});

router.post('/updateVideogame', function(req, res, next) {
	var db = req.con;
    db.query('UPDATE User SET name = "VideogameTestUpdate" WHERE name = "VideogameTest" ', function(err,rows){
        if(err) throw err;
        res.end();
    });
});

//CRUD OPERATION PLATFORM
router.post('/insertPlatform', function(req, res, next) {
	var db = req.con;

	var name = req.body.inputName;
    
	//QUERY INSERT PLATFORM
	db.query('INSERT INTO Platform (name) VALUES ("'+name+'")', function(err,rows){
		if(err) throw err;
	});
    //QUERY SEARCH PLATFORM THAT WAS JUST ADDED
    db.query('SELECT * FROM Platform WHERE name = "PlatformTest"', function(err,rows){
		if(err) throw err;
		res.end();
    });
});

router.post('/deletePlatform', function(req, res, next) {
	var db = req.con;
    db.query('DELETE FROM Platform WHERE name = "PlatformTest" ', function(err,rows){
        if(err) throw err;
        res.end();
    });
});

router.post('/updatePlatform', function(req, res, next) {
	var db = req.con;
    db.query('UPDATE Platform SET name = "PlatformTestUpdate" WHERE name = "PlatformTest" ', function(err,rows){
        if(err) throw err;
        res.end();
    });
});

//CRUD OPERATION GENRE
router.post('/insertGenre', function(req, res, next) {
	var db = req.con;

	var type = req.body.inputType;
    
	//QUERY INSERT PLATFORM
	db.query('INSERT INTO Genre (type) VALUES ("'+type+'")', function(err,rows){
		if(err) throw err;
	});
    //QUERY SEARCH PLATFORM THAT WAS JUST ADDED
    db.query('SELECT * FROM Genre WHERE type = "GenreTest"', function(err,rows){
		if(err) throw err;
		res.end();
    });
});

router.post('/deleteGenre', function(req, res, next) {
	var db = req.con;
    db.query('DELETE FROM Genre WHERE type = "GenreTest" ', function(err,rows){
        if(err) throw err;
        res.end();
    });
});

router.post('/updateGenre', function(req, res, next) {
	var db = req.con;
    db.query('UPDATE Genre SET type = "GenreTestUpdate" WHERE type = "GenreTest" ', function(err,rows){
        if(err) throw err;
        res.end();
    });
});

module.exports = router;
