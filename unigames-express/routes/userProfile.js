var express = require('express');
var router = express.Router();
var moment = require('moment');
var uuid4 = require('uuid4');

var crypto = require('crypto'),
algorithm = 'aes-256-ctr',
password = 'd6F3Efeq';

function encrypt(text){
	var cipher = crypto.createCipher(algorithm,password)
	var crypted = cipher.update(text,'utf8','hex')
	crypted += cipher.final('hex');
	return crypted;
  }

router.get('/', function (req, res, next) {
    var date = req.session.user[0].dateOfBirth;
    var newDate = moment(date).format("DD-MM-YYYY");
	res.render('userProfile', {newDate:newDate});
});

router.get('/wallet', function (req, res, next) {
    var db = req.con;
    var idWallet =  req.session.user[0].idWallet;
    var realBalance;
    var virtualBalance;
    //var idUser = req.session.user[0].idUser;
    db.query('SELECT realBalance, virtualBalance FROM Wallet WHERE idWallet = ?',[idWallet], function(err,result){
        realBalance = result[0].realBalance;
        virtualBalance = result[0].virtualBalance;
        db.query('SELECT idCreditCard, cvv, expiredDate FROM creditcard WHERE owner = ?',[idWallet], function(err,result){
            creditCardData = result;
            res.render('wallet', {realBalance:realBalance, virtualBalance:virtualBalance, creditCardData:creditCardData});
        });

    });
});

router.get('/rechargeWallet', function (req, res, next) {
    var db = req.con;
    var idWallet = req.session.user[0].idWallet;
    db.query('SELECT idCreditCard, cvv, expiredDate FROM creditcard WHERE owner = ?',[idWallet], function(err,result){
        creditCardData = result;
        console.log(creditCardData);
        res.render('rechargeWallet', {creditCardData:creditCardData});
    });
});

router.get('/rechargeWalletRecharge', function (req, res, next) {
    res.render('rechargeWalletRecharge');
});

router.post('/rechargeWalletRecharge', function (req, res, next){
    var db = req.con;
    var money = parseFloat(req.body.Amount);
    console.log(money);
    var idUser = req.session.user[0].idUser;

    db.query('SELECT idWallet FROM User WHERE idUser = ?',[idUser], function(err,rows){
        var idWallet = rows[0].idWallet;
        console.log(idWallet);
        db.query('SELECT realBalance FROM Wallet WHERE idWallet = ? ',[idWallet], function(err,rows){
            realBalance = rows[0].realBalance;
            money = realBalance + money;
            db.query('UPDATE Wallet SET realBalance = ? WHERE idWallet = ? ',[money, idWallet], function(err,rows){
                res.redirect('wallet');
            });
        });

    });


});

router.get('/creditCard', function (req, res, next) {
    res.render('creditCard');
});

router.get('/editCreditCard/:id', function(req,res,next){
	var db = req.con;
    var idCreditCard = req.params.id;
    var idUser = req.session.user[0].idUser;
    console.log(idCreditCard);
    var notPermission;
    db.query('SELECT owner FROM creditcard WHERE idCreditCard = ? ',[idCreditCard], function(err,idWalletQuery){
        if(err) throw err;
        db.query('SELECT idUser FROM user WHERE idWallet = ?', [idWalletQuery[0].owner], function(err,idUserQuery){
            if(err) throw err;
            if (idUserQuery[0].idUser == idUser) {
                db.query('SELECT * FROM creditcard WHERE idCreditCard = ?',[idCreditCard], function(err,creditCard){
                    if(err) throw err;
                    notPermission = 0;
                    var creditCard = creditCard;
                    var date = moment(creditCard[0].expiredDate).format('DD-MM-YYYY');
                    console.log(date);
                    var year = date.toString().substring(8,10);
                    console.log(year);
                    var month = date.toString().substring(3,5);
                    console.log(month);
                    console.log(creditCard);
                    res.render('editCreditCard', {creditCard:creditCard, year:year, month:month, notPermission: notPermission});
                });
            }
            else{
                notPermission = 1;
                res.render('editCreditCard', {notPermission : notPermission});
            }
        });
    });


});

router.post('/editCreditCard/:id', function(req,res,next){
	var db = req.con;
    var idCreditCard = req.params.id;

    var creditCardNumber = req.body.creditcard;
    var creditCardNumber = creditCardNumber.replace(/\s/g, '');
    console.log(creditCardNumber);
    var cvv = req.body.cccvc;
    var expireDate = req.body.ccexp;
    var month = expireDate.substring(0,2);
    var year = expireDate.substring(5,7);
    var fullYear = "20" + year;
    var fullDate = fullYear + "-" + month + "-" + "01";
    console.log(fullDate);
    console.log(expireDate);

    db.query('UPDATE creditcard SET idCreditCard = ?, cvv = ?, expiredDate = ? WHERE idCreditCard = ?',[creditCardNumber, cvv, fullDate, idCreditCard], function(err,creditCard){
        if(err) throw err;
        res.redirect('/userProfile/wallet');
    });
});

router.get('/removeCreditCard/:id', function(req,res,next){
    var idCreditCard = req.params.id;
    var notPermission;
    var db = req.con;
    var idUser = req.session.user[0].idUser;
    db.query('SELECT owner FROM creditcard WHERE idCreditCard = ? ',[idCreditCard], function(err,idWalletQuery){
        if(err) throw err;
        db.query('SELECT idUser FROM user WHERE idWallet = ?', [idWalletQuery[0].owner], function(err,idUserQuery){
            if(err) throw err;
            if (idUserQuery[0].idUser == idUser) {
                notPermission = 0;
                res.render('removeCreditCard', {idCreditCard:idCreditCard, notPermission : notPermission})
            }
            else{
                notPermission = 1;
                res.render('removeCreditCard', {notPermission : notPermission});
            }
        });
    });
});

router.post('/removeCreditCard/:id', function(req,res,next){
	var db = req.con;
    var idCreditCard = req.params.id;

    db.query('DELETE FROM creditcard WHERE idCreditCard = ?',[idCreditCard], function(err,creditCard){
        if(err) throw err;
        res.redirect('/userProfile/wallet');
    });
});

router.post('/creditCardCheck', function(req,res,next){
    var db = req.con;
    var creditCardNumber = req.body.creditCardNumber;
    console.log(creditCardNumber);

    db.query('SELECT * FROM creditcard WHERE idCreditCard = ?',[creditCardNumber], function(err,results){
        if(err) throw err;
        res.send(results);
    });
});


router.get('/updatePassword', function (req, res, next) {
	res.render('updatePassword');
});

router.get('/updateEmail', function (req, res, next) {
	res.render('updateEmail');
});

router.get('/confirmUpdateEmail', function (req, res, next) {
	res.render('confirmUpdateEmail');
});

router.get('/confirmUpdatePassword', function (req, res, next) {
	res.render('confirmUpdatePassword');
});

router.get('/updateUser', function (req, res, next) {
    var date = req.session.user[0].dateOfBirth;
    var newDate = moment(date).format("YYYY-MM-DD");
    res.render('updateUser', {newDate:newDate});
});

router.post('/updateUser', function (req, res, next){
	var db = req.con;

    var idUser = req.session.user[0].idUser;
	var name = req.body.inputName;
	var surname = req.body.inputSurname;
	var dateOfBirth = req.body.inputDateOfBirth;
	var placeOfBirth = req.body.inputPlaceOfBirth;
	var address = req.body.inputAddress;
	var civicNumber = req.body.inputCivicNumber;
	var city = req.body.inputCity;
	var province = req.body.inputProvince;
	var phoneNumber = req.body.inputPhoneNumber;

    db.query('UPDATE User SET name = ?, surname = ?, dateOfBirth = ?, placeOfBirth = ?, address = ?, civicNumber = ?, city = ?, province = ?, phoneNumber = ? WHERE idUser = ? ',[name, surname, dateOfBirth, placeOfBirth, address, civicNumber, city, province, phoneNumber, idUser], function(err,rows){
        if(err) throw err;
        req.session.user[0].name = name;
        req.session.user[0].surname = surname;
        req.session.user[0].dateOfBirth = dateOfBirth;
        req.session.user[0].placeOfBirth = placeOfBirth;
        req.session.user[0].address = address;
        req.session.user[0].civicNumber = civicNumber;
        req.session.user[0].city = city;
        req.session.user[0].province = province;
        req.session.user[0].phoneNumber = phoneNumber;
        sess = req.session;
        res.redirect('/userProfile');
        //res.end();
    });

});

router.post('/updatePassword', function (req, res, next){
    var db = req.con;
    var idUser = req.session.user[0].idUser;
    var password = encrypt(req.body.inputPassword1);

    db.query('UPDATE User SET password = ? WHERE idUser = ? ',[password, idUser], function(err,rows){
        if(err) throw err;

        req.session.loggedin = false;
        res.redirect('confirmUpdatePassword');
        //res.end();
    });

});

router.post('/updateEmail', function (req, res, next){
    var db = req.con;

    var idUser = req.session.user[0].idUser;
    var email = req.body.inputEmail;

    db.query('UPDATE User SET email = ? WHERE idUser = ? ',[email, idUser], function(err,rows){
        if(err) throw err;

        req.session.loggedin = false;
        res.redirect('confirmUpdateEmail');
        res.end();
    });

});

router.post('/creditCard', function (req, res, next){
    var db = req.con;

    var idWallet =  req.session.user[0].idWallet;
    var creditCardNumber = req.body.creditcard;
    var creditCardNumber = creditCardNumber.replace(/\s/g, '');
    console.log(creditCardNumber);
    var cvv = req.body.cccvc;
    var expireDate = req.body.ccexp;
    var month = expireDate.substring(0,2);
    var year = expireDate.substring(5,7);
    var fullYear = "20" + year;
    var fullDate = fullYear + "-" + month + "-" + "01";
    console.log(fullDate);
    console.log(expireDate);

    db.query('INSERT INTO creditcard (idCreditCard, cvv, expiredDate, owner) VALUES ("'+creditCardNumber+'", "'+cvv+'", "'+fullDate+'", "'+idWallet+'")', function(err,rows){
        if(err) throw err;
        res.redirect('wallet');
		res.end();
    });

});

router.post('/updateEmailTEST', function (req, res, next){
    var db = req.con;

    var idUser = req.body.idUser;
    var email = req.body.inputEmail;

    db.query('UPDATE User SET email = ? WHERE idUser = ? ',[email, idUser], function(err,rows){
        if(err) throw err;

        req.session.loggedin = false;
        //res.redirect('confirmUpdateEmail');
        res.end();
    });

});

router.post('/updatePasswordTEST', function (req, res, next){
    var db = req.con;
    var idUser = req.body.idUser;
    var password = encrypt(req.body.inputPassword1);

    db.query('UPDATE User SET password = ? WHERE idUser = ? ',[password, idUser], function(err,rows){
        if(err) throw err;

        req.session.loggedin = false;
        res.redirect('confirmUpdatePassword');
        //res.end();
    });

});


router.post('/updateUserTEST', function (req, res, next){
	var db = req.con;

    var idUser = req.body.idUser;
	var name = req.body.inputName;
	var surname = req.body.inputSurname;
	var dateOfBirth = req.body.inputDateOfBirth;
	var placeOfBirth = req.body.inputPlaceOfBirth;
	var address = req.body.inputAddress;
	var civicNumber = req.body.inputCivicNumber;
	var city = req.body.inputCity;
	var province = req.body.inputProvince;
	var phoneNumber = req.body.inputPhoneNumber;

    db.query('UPDATE User SET name = ?, surname = ?, dateOfBirth = ?, placeOfBirth = ?, address = ?, civicNumber = ?, city = ?, province = ?, phoneNumber = ? WHERE idUser = ? ',[name, surname, dateOfBirth, placeOfBirth, address, civicNumber, city, province, phoneNumber, idUser], function(err,rows){
        if(err) throw err;
    });

    //QUERY SEARCH USER THAT WAS JUST MODIFIED
    db.query('SELECT * FROM User WHERE name = ?',[name], function(err,rows){
        if(err) throw err;
        res.end();
    });
});

router.get('/wishlist', function(req,res,next){
    sess=req.session;
    var db = req.con;

    if (typeof sess.confirmationInserted == 'undefined')
        p = "";
    else
        p = sess.confirmationInserted;

    if (typeof sess.closeAlert == 'undefined')
        c = "";
    else
        c = sess.closeAlert;

    db.query("SELECT idUser FROM User WHERE email=?",[sess.email],function(err,rows){
        if(err) throw err;

        var fields = JSON.parse(JSON.stringify(rows[0]));
        var idUser = fields.idUser;

        db.query("SELECT * FROM Wishlist WHERE idUser=?",[idUser],function(err,wishlist){
            if(err) throw err;

            db.query("SELECT * FROM Videogame",function(err,vg){
                if(err) throw err;

                db.query('SELECT * FROM Genre',function(err,genres){
                    if(err) throw err;

                    db.query('SELECT * FROM HasGenre', function(err,hasgenre){
                        if(err) throw err;

                        db.query('SELECT * FROM Review', function(err,rev){
                            if(err) throw err;

                            res.render('wishlist', {vg:vg,wl:wishlist,gn:genres, hasg:hasgenre, review:rev,confirmationInserted:p, closeAlert:c});
                        });
                    });
                });
            });
        });
    });
});

router.get('/removeVideogame/:id',function(req,res,next){
    sess=req.session;
    var db=req.con;
    var idVideogame = req.params.id;

    db.query("SELECT idUser FROM User WHERE email=?",[sess.email],function(err,rows){
        if(err) throw err;

        var fields = JSON.parse(JSON.stringify(rows[0]));
        var idUser = fields.idUser;

        db.query("DELETE FROM Wishlist WHERE idUser='"+idUser+"' AND idVideogame="+idVideogame, function(err,rows){
            if(err) throw err;

            req.session.confirmationInserted = "Il videogioco è stato rimosso dalla tua Lista dei Desideri";
            req.session.closeAlert = "X";
            res.redirect('/userProfile/wishlist');
        });
    });
});



module.exports = router;
