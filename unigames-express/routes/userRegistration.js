var express = require('express');
var router = express.Router();
var uuid4 = require('uuid4');


var crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'd6F3Efeq';

function encrypt(text){
  var cipher = crypto.createCipher(algorithm,password)
  var crypted = cipher.update(text,'utf8','hex')
  crypted += cipher.final('hex');
  return crypted;
}
 
router.get('/', function (req, res, next) {
	sess = req.session;
	res.render('userRegistration');
});

router.post('/check', function (req, res, next) {
	var db = req.con;
	var email = req.body.email;
	db.query('SELECT * FROM User WHERE email = ?',[email], function(err,results, rows){
		if(err) throw err;
		res.send(results);
	});
});

router.get('/confirmRegistration', function(req, res, next){
	res.render('confirmRegistration');
});

router.post('/user', function(req, res, next) {
	var db = req.con;
	var idUser = uuid4();
	var idWallet = uuid4();
	var idSubscription = "";
	if (req.body.checkSubscription == 'deluxe'){
		var idSubscription = "DLX";
	}else{
		var idSubscription = "BSC";
	}

	var name = req.body.inputName;
	var surname = req.body.inputSurname;
	var dateOfBirth = req.body.inputDateOfBirth;
	var placeOfBirth = req.body.inputPlaceOfBirth;
	var address = req.body.inputAddress;
	var civicNumber = req.body.inputCivicNumber;
	var city = req.body.inputCity;
	var province = req.body.inputProvince;
	var email = req.body.inputEmail;
	var password = encrypt(req.body.inputPassword1);
	var phoneNumber = req.body.inputPhoneNumber;

	var realBalance = 0;
	var virtualBalance = 0;

	//QUERY INSERT WALLET
	db.query('INSERT INTO Wallet (idWallet, realBalance, virtualBalance) VALUES ("'+idWallet+'","'+realBalance+'","'+virtualBalance+'")', function(err,rows){
		if(err) throw err;
	});
	//QUERY INSERT USER
	db.query('INSERT INTO User (idUser, name, surname, dateOfBirth, placeOfBirth, address, civicNumber, city, province, email, password, phoneNumber, admin, idSubscription, idWallet) VALUES ("'+idUser+'","'+name+'", "'+surname+'", "'+dateOfBirth+'", "'+placeOfBirth+'", "'+address+'", "'+civicNumber+'", "'+city+'", "'+province+'", "'+email+'", "'+password+'", "'+phoneNumber+'", 0, "'+idSubscription+'", "'+idWallet+'")', function(err,rows){
		if(err) throw err;
		res.redirect('confirmRegistration');
		res.end();
	});
});

module.exports = router;
