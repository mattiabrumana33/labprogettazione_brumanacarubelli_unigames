var express = require('express');
var router = express.Router();

router.get('/:id', function(req, res, next) {
  sess=req.session;
	var db = req.con;
  let id = req.params.id;

  if (typeof sess.confirmationInserted == 'undefined')
    p = "";
  else
    p = sess.confirmationInserted;

  if (typeof sess.closeAlert == 'undefined')
    ca = "";
  else
    ca = sess.closeAlert;

	db.query('SELECT * FROM Videogame WHERE idVideogame = ?', [id], function(err,videogame){
        if(err) throw err;
        db.query('SELECT * FROM HasGenre WHERE idVideogame = ?', [id], function(err, idGenres){
          if (err) throw err;
          var arr = idGenres.map( function(el) { return el.idGenre; });
            db.query('SELECT type FROM Genre WHERE idGenre IN (?)', [arr], function(err, genre){
              if (err) throw err;
              db.query('SELECT * FROM PlayableOn WHERE idVideogame = ?', [id], function(err, idPlatforms){
                if (err) throw err;
                var arr2 = idPlatforms.map( function(el) { return el.idPlatform; });
                db.query('SELECT name FROM Platform WHERE idPlatform IN (?)', [arr2], function(err, platform){
                  if (err) throw err;
                  db.query('SELECT * FROM Review WHERE idVideogame=? ORDER BY creationDate DESC',[id],function(err,revs){
                    if(err) throw err;
                    db.query('SELECT * FROM User',function(err,u){
                      if(err) throw err;

                      res.render('videogame', {videogame:videogame,genre:genre,platform:platform,reviews:revs,user:u,c:p,ca:ca});
                    });
                  });
                });
              });
            });
        });
    });
});

router.post('/insertReview', function(req,res,next){
  sess=req.session;
  var db = req.con;
  var idV = req.body.inputVideogameId;
  var text = req.body.inputText;
  var rating = req.body.rating;

  db.query('SELECT idUser FROM User WHERE email=?',[sess.email],function(err,rows){
    if(err) throw err;
    var fields = JSON.parse(JSON.stringify(rows[0]));
		var idU = fields.idUser;

    db.query('INSERT INTO Review (idVideogame, idUser, text, stars) VALUES ("'+idV+'", "'+idU+'", "'+text+'", "'+rating+'")', function(err,rows){
      if(err) throw err;
    });
  });

  req.session.confirmationInserted = "La recensione è stata inserita con successo.";
  req.session.closeAlert = "X";
  res.redirect('/videogame/' + idV);
});

router.post('/deleteVariables', function(res,req,next){
  delete sess.confirmationInserted;
  delete sess.closeAlert;
  sess.save();
  req.end();
});

module.exports = router;