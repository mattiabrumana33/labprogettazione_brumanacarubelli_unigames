var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
	sess=req.session;
	var db = req.con;

	db.query('SELECT * FROM Videogame',function(err,vg){
		if(err) throw err;

		if (typeof sess.confirmationInserted == 'undefined')
            p = "";
        else
            p = sess.confirmationInserted;

        if (typeof sess.closeAlert == 'undefined')
            c = "";
        else
			c = sess.closeAlert;

			db.query('SELECT * FROM Genre',function(err,genres){
			if(err) throw err;

			db.query('SELECT * FROM HasGenre', function(err,hasgenre){
				if(err) throw err;

				db.query('SELECT * FROM Review', function(err,rev){
					if(err) throw err;
					res.render('videogames', {vg:vg, gn:genres, hasg:hasgenre, review:rev, confirmationInserted:p, closeAlert:c});
				});
			});
		});
	});
});

router.get('/videogamesAdmin', function(req, res, next) {
	sess=req.session;
	var db = req.con;
	db.query('SELECT * FROM Videogame',function(err,vg){
		if(err) throw err;

		if (typeof sess.confirmationInserted == 'undefined')
            p = "";
        else
            p = sess.confirmationInserted;

        if (typeof sess.closeAlert == 'undefined')
            c = "";
        else
			c = sess.closeAlert;

		res.render('videogamesAdmin', {vg:vg, confirmationInserted:p, closeAlert:c});
	});
});

router.get('/newVideogame', function(req, res, next) {
	sess=req.session;
	var db = req.con;
	db.query('SELECT * FROM Platform', function(err,rows_pl){
		if(err) throw err;
		db.query('SELECT * FROM Genre', function(err,rows_gn){
			if(err) throw err;

			if (typeof sess.confirmationInserted == 'undefined')
            p = "";
			else
				p = sess.confirmationInserted;

			if (typeof sess.closeAlert == 'undefined')
				c = "";
			else
				c = sess.closeAlert;

			res.render('newVideogame', {data_pl:rows_pl, data_gn:rows_gn, confirmationInserted:p, closeAlert:c});
		});
	});
});

router.get('/editVideogame/:id', function(req,res,next){
	sess=req.session;
	var db = req.con;
	var idVideogame = req.params.id;
	db.query('SELECT * FROM Videogame WHERE idVideogame=?',[idVideogame], function(err,videogame){
		if(err) throw err;

		db.query('SELECT * FROM Platform', function(err,all_platforms){
			if(err) throw err;
			db.query('SELECT idPlatform FROM PlayableOn WHERE idVideogame=?',[idVideogame],function(err,platforms_selected){
				if(err) throw err;
				db.query('SELECT * FROM Genre', function(err,all_genres){
					if(err) throw err;
					db.query('SELECT idGenre FROM HasGenre WHERE idVideogame=?',[idVideogame],function(err,genres_selected){
						if(err) throw err;

						if (typeof sess.confirmationInserted == 'undefined')
							p = "";
						else
							p = sess.confirmationInserted;

						if (typeof sess.closeAlert == 'undefined')
							c = "";
						else
							c = sess.closeAlert;
						res.render('editVideogame', {vg:videogame,all_pl:all_platforms,sel_pl:platforms_selected,
							all_gn:all_genres,sel_gn:genres_selected, confirmationInserted:p, closeAlert:c});
					});
				});
			});
		});
	});
});

router.get('/removeVideogame/:id', function(req,res,next){
	sess=req.session;
	var db=req.con;
	var idVideogame = req.params.id;
	db.query('DELETE FROM PlayableOn WHERE idVideogame=?',[idVideogame],function(err,rows){
		if(err) throw err;
	});

	db.query('DELETE FROM Videogame WHERE idVideogame=?',[idVideogame],function(err,rows){
		if(err) throw err;
	});

	req.session.confirmationInserted = "Il videogioco è stato rimosso con successo.";
    req.session.closeAlert = "X";
    res.redirect('/videogames/videogamesAdmin');
});

router.post('/newVideogame/checkAvailability', function (req, res, next) {
	var db = req.con;
	var title = req.body.title;
	db.query('SELECT * FROM Videogame WHERE name = ?',[title], function(err,results,rows){
		if(err) throw err;
		res.send(results);
		res.end();
	});
});

router.post('/editVideogame/checkAvailability', function (req, res, next) {
	var db = req.con;
	var id = req.body.id;
	var name = req.body.name;
	db.query('SELECT * FROM Videogame WHERE name = ?',[name], function(err,results,rows){
		if(err) throw err;

		if(results.length > 0){
			var fields = JSON.parse(JSON.stringify(results[0]));
			var idVideogame = fields.idVideogame;

			if(id != idVideogame){
				results = [];
			}
		}

		res.send(results);
		res.end();
	});
});

router.post('/deleteVariables', function(res,req,next){
    delete sess.confirmationInserted;
    delete sess.closeAlert;
    sess.save();
    req.end();
});

router.post('/newVideogame', function(req, res, next) {
	var db = req.con;
	var name = req.body.inputTitle;
	var releaseYear = req.body.inputReleaseYear;
	var softwareHouse = req.body.inputSoftwareHouse;
	var description = req.body.inputDescription;
	var buyPrice = req.body.inputBuyPrice;
	var rentPrice = req.body.inputRentPrice;
	var platforms = req.body.inputPlatforms;
	var genres = req.body.inputGenres;

	db.query('INSERT INTO Videogame (name, releaseYear, softwareHouse, description, buyPrice, rentPrice) VALUES ("'+name+'", "'+releaseYear+'", "'+softwareHouse+'", "'+description+'", "'+buyPrice+'", "'+rentPrice+'")', function(err,rows){
        if(err) throw err;
	});

	db.query('SELECT idVideogame FROM Videogame WHERE name = ?',[name], function(err,results){
		if(err) throw err;
		var fields = JSON.parse(JSON.stringify(results[0]));
		var idVideogame = fields.idVideogame;

		if(platforms != null){
			for(var i=0;i<platforms.length;i++){
				db.query('INSERT INTO PlayableOn (idVideogame, idPlatform) VALUES ('+idVideogame+', '+platforms[i]+')', function(err,rows){
					if(err) throw err;
				});
			}
		}

		if(genres != null){
			for(var j=0;j<genres.length;j++){
				db.query('INSERT INTO HasGenre (idVideogame, idGenre) VALUES ('+idVideogame+', '+genres[j]+')', function(err,rows){
					if(err) throw err;
				});
			}
		}
	});

	req.session.confirmationInserted = "Il videogioco è stato inserito con successo.";
    req.session.closeAlert = "X";
    res.redirect('/videogames/videogamesAdmin');
    res.end();
});

router.post('/editVideogame', function(req, res, next) {
	var db = req.con;
	var idVideogame = req.body.inputId;
	var name = req.body.inputTitle;
	var releaseYear = req.body.inputReleaseYear;
	var softwareHouse = req.body.inputSoftwareHouse;
	var description = req.body.inputDescription;
	var buyPrice = req.body.inputBuyPrice;
	var rentPrice = req.body.inputRentPrice;
	var platforms = req.body.inputPlatforms;
	var genres = req.body.inputGenres;

	db.query('UPDATE Videogame SET name=?,releaseYear=?,softwareHouse=?,description=?,buyPrice=?,rentPrice=? WHERE idVideogame=?',[name,releaseYear,softwareHouse,description,buyPrice,rentPrice,idVideogame], function(err,rows){
        if(err) throw err;
	});

	db.query('DELETE FROM PlayableOn WHERE idVideogame=?',[idVideogame], function(err,rows){
		if(err) throw err;
	});

	if (platforms != null){
		for(var i=0;i<platforms.length;i++){
			db.query('INSERT INTO PlayableOn (idVideogame, idPlatform) VALUES ('+idVideogame+', '+platforms[i]+')', function(err,rows){
				if(err) throw err;
			});
		}
	}

	db.query('DELETE FROM HasGenre WHERE idVideogame=?',[idVideogame], function(err,rows){
		if(err) throw err;
	});

	if (genres != null){
		for(var j=0;j<genres.length;j++){
			db.query('INSERT INTO HasGenre (idVideogame, idGenre) VALUES ('+idVideogame+', '+genres[j]+')', function(err,rows){
				if(err) throw err;
			});
		}
	}

    req.session.confirmationInserted = "Il videogioco è stato modificato con successo.";
    req.session.closeAlert = "X";
    res.redirect('/videogames/videogamesAdmin');
    res.end();
});

router.post('/search', function(req,res,next){
	var db = req.con;
	var searched_param = req.body.inputSearch;
	var search_param_filter = req.body.inputSearchParameter;
	var searched_column;

	if (typeof sess.confirmationInserted == 'undefined')
		p = "";
	else
		p = sess.confirmationInserted;

	if (typeof sess.closeAlert == 'undefined')
		c = "";
	else
		c = sess.closeAlert;


	switch(search_param_filter) {
		case 'searchTitle':
			searched_column = 'name';
			break;
		case 'searchReleaseYear':
			searched_column = "releaseYear";
			break;
		case 'searchSoftwareHouse':
			searched_column = "softwareHouse";
			break;
		case 'searchDescription':
			searched_column = "description";
			break;
		case 'searchPlatform':
			searched_column = "platform";
			break;
		case 'searchGenre':
			searched_column = "genre";
			break;
	}

	if(searched_column == "platform") {
		db.query('SELECT v.idVideogame, v.name, v.releaseYear, v.softwareHouse, v.description, v.buyPrice, v.rentPrice, v.image, v.trailer FROM Videogame as v, Platform as p, PlayableOn as po WHERE v.idVideogame=po.idVideogame AND p.idPlatform=po.idPlatform AND p.name LIKE "%' + searched_param + '%"', function(err,vg){
			if(err) throw err;

			db.query('SELECT * FROM Genre',function(err,genres){
				if(err) throw err;

				db.query('SELECT * FROM HasGenre', function(err,hasgenre){
					if(err) throw err;

					db.query('SELECT * FROM Review', function(err,rev){
						if(err) throw err;
						res.render('videogames', {vg:vg, gn:genres, hasg:hasgenre, review:rev, confirmationInserted:p, closeAlert:c});
					});
				});
			});
		});
	}

	if(searched_column == "genre") {
		db.query('SELECT * FROM Videogame as v, Genre as g, HasGenre as h WHERE v.idVideogame=h.idVideogame AND g.idGenre=h.idGenre AND g.type LIKE "%' + searched_param + '%"', function(err,vg){
			if(err) throw err;

			db.query('SELECT * FROM Genre',function(err,genres){
				if(err) throw err;

				db.query('SELECT * FROM HasGenre', function(err,hasgenre){
					if(err) throw err;
					db.query('SELECT * FROM Review', function(err,rev){
						if(err) throw err;
						res.render('videogames', {vg:vg, gn:genres, hasg:hasgenre, review:rev, confirmationInserted:p, closeAlert:c});
					});
				});
			});
		});
	}

	if((searched_column != "platform") && (searched_column != "genre")){
		db.query('SELECT * FROM Videogame WHERE ' + searched_column + ' LIKE "%' + searched_param + '%"', function(err,vg){
			if(err) throw err;

			db.query('SELECT * FROM Genre',function(err,genres){
				if(err) throw err;

				db.query('SELECT * FROM HasGenre', function(err,hasgenre){
					if(err) throw err;
					db.query('SELECT * FROM Review', function(err,rev){
						if(err) throw err;
						res.render('videogames', {vg:vg, gn:genres, hasg:hasgenre, review:rev, confirmationInserted:p, closeAlert:c});
					});
				});
			});
		});
	}
});

router.post('/searchps4', function(req, res, next) {
	var db = req.con;
	var searched_param = "ps4";

	if (typeof sess.confirmationInserted == 'undefined')
		p = "";
	else
		p = sess.confirmationInserted;

	if (typeof sess.closeAlert == 'undefined')
		c = "";
	else
		c = sess.closeAlert;


		db.query('SELECT v.idVideogame, v.name, v.releaseYear, v.softwareHouse, v.description, v.buyPrice, v.rentPrice, v.image, v.trailer FROM Videogame as v, Platform as p, PlayableOn as po WHERE v.idVideogame=po.idVideogame AND p.idPlatform=po.idPlatform AND p.name LIKE "%' + searched_param + '%"', function(err,vg){
			if(err) throw err;

			db.query('SELECT * FROM Genre',function(err,genres){
				if(err) throw err;

				db.query('SELECT * FROM HasGenre', function(err,hasgenre){
					if(err) throw err;
					db.query('SELECT * FROM Review', function(err,rev){
						if(err) throw err;
						res.render('videogames', {vg:vg, gn:genres, hasg:hasgenre, review:rev, confirmationInserted:p, closeAlert:c});
					});
				});
			});
		});
});

router.post('/searchNintendoSwitch', function(req, res, next) {
	var db = req.con;
	var searched_param = "Nintendo Switch";

	if (typeof sess.confirmationInserted == 'undefined')
		p = "";
	else
		p = sess.confirmationInserted;

	if (typeof sess.closeAlert == 'undefined')
		c = "";
	else
		c = sess.closeAlert;


	db.query('SELECT v.idVideogame, v.name, v.releaseYear, v.softwareHouse, v.description, v.buyPrice, v.rentPrice, v.image, v.trailer FROM Videogame as v, Platform as p, PlayableOn as po WHERE v.idVideogame=po.idVideogame AND p.idPlatform=po.idPlatform AND p.name LIKE "%' + searched_param + '%"', function(err,vg){
		if(err) throw err;

		db.query('SELECT * FROM Genre',function(err,genres){
			if(err) throw err;

			db.query('SELECT * FROM HasGenre', function(err,hasgenre){
				if(err) throw err;

				db.query('SELECT * FROM Review', function(err,rev){
					if(err) throw err;
					res.render('videogames', {vg:vg, gn:genres, hasg:hasgenre, review:rev, confirmationInserted:p, closeAlert:c});
				});
			});
		});
	});
});

router.post('/searchXboxOne', function(req, res, next) {
	var db = req.con;
	var searched_param = "Xbox One";

	if (typeof sess.confirmationInserted == 'undefined')
		p = "";
	else
		p = sess.confirmationInserted;

	if (typeof sess.closeAlert == 'undefined')
		c = "";
	else
		c = sess.closeAlert;


	db.query('SELECT v.idVideogame, v.name, v.releaseYear, v.softwareHouse, v.description, v.buyPrice, v.rentPrice, v.image, v.trailer FROM Videogame as v, Platform as p, PlayableOn as po WHERE v.idVideogame=po.idVideogame AND p.idPlatform=po.idPlatform AND p.name LIKE "%' + searched_param + '%"', function(err,vg){
		if(err) throw err;

		db.query('SELECT * FROM Genre',function(err,genres){
			if(err) throw err;

			db.query('SELECT * FROM HasGenre', function(err,hasgenre){
				if(err) throw err;
				db.query('SELECT * FROM Review', function(err,rev){
					if(err) throw err;
					res.render('videogames', {vg:vg, gn:genres, hasg:hasgenre, review:rev, confirmationInserted:p, closeAlert:c});
				});
			});
		});
	});
});

router.get('/addToWishlist/:id', function(req,res,next){
	sess=req.session;
	var db = req.con;
	var idVideogame = req.params.id;

	if (typeof sess.email == 'undefined') {
		req.session.confirmationInserted = "Accedi per continuare.";
		req.session.closeAlert = "X";
		res.redirect('/videogames');
	} else {
		db.query('SELECT idUser FROM User WHERE email=?',[sess.email],function(err,rows){
			if(err) throw err;

			var fields = JSON.parse(JSON.stringify(rows[0]));
			var idUser = fields.idUser;

			db.query('INSERT INTO Wishlist(idUser, idVideogame) VALUES("'+idUser+'", '+idVideogame+')',function(err,rows){
				if (err) {
					req.session.confirmationInserted = "Il videogioco è già presente nella Lista dei Desideri.";
					req.session.closeAlert = "X";
					res.redirect('/videogames');
				} else {
					req.session.confirmationInserted = "Aggiunto alla Lista dei Desideri.";
					req.session.closeAlert = "X";
					res.redirect('/videogames');
				}
			});
		});
	}
});


module.exports = router;
