const request = require('supertest')
const app = require('../app_test')

//Genres GET
describe('Genres GET', () => {
    it('should test that true = true', () => {
        expect(true).toBe(true)
    })
})

//New Genre POST
describe('New Genre POST', () => {
    it('should create a new genre', (done) => {
        request(app)
            .post('/genres/newGenre')
            .send({
                inputType: '_ProvaGenere_',
                inputVideogames: ['1', '2', '3']
            })
            .expect(302, done)
    })
})

//Edit Genre POST
describe('Edit Genre POST', () => {
    it('should modify an existing genre', (done) => {
        request(app)
            .post('/genres/editGenre')
            .send({
                inputId: '3',
                inputType: 'GenereAzioneModificato',
                inputVideogames: ['4', '5', '6']
            })
            .expect(302, done)
    })
})

//Remove Genre GET
describe('Remove Genre GET', () => {
    it('should remove an existing genre', (done) => {
        request(app)
            .get('/genres/removeGenre/4')
            .expect(302, done)
    })
})
