const request = require('supertest')
const app = require('../app_test')

//Platforms GET
describe('Platforms GET', () => {
    it('should test that true = true', () => {
        expect(true).toBe(true)
    })
})

//New Platform POST
describe('New Platform POST', () => {
    it('should create a new platform', (done) => {
        request(app)
            .post('/platforms/newPlatform')
            .send({
                inputTitle: '_ProvaPiattaforma_',
                inputVideogames: ['1', '2', '3']
            })
            .expect(302, done)
    })
})

//Edit Platform POST
describe('Edit Platform POST', () => {
    it('should modify an existing platform', (done) => {
        request(app)
            .post('/platforms/editPlatform')
            .send({
                inputId: '14',
                inputTitle: '_ProvaPiattaforma9Modificata',
                inputVideogames: ['4', '5', '6']
            })
            .expect(302, done)
    })
})

//Remove Platform GET
describe('Remove Platform GET', () => {
    it('should remove an existing platform', (done) => {
        request(app)
            .get('/platforms/removePlatform/15')
            .expect(302, done)
    })
})
