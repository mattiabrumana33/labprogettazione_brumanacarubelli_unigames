const request = require('supertest')
const app = require('../app_test')

// TEST CONNECTION TO DATABASE
describe('TEST CONNECTION TO DATABASE', () => {
    it('connecting...', (done) => {
      request(app)
        .post('/testDB/connection')
        // .expect(response => {console.log("Connection established!")})
        .expect(200, done)
    })
})

//CRUD OPERATION USER
//create
describe('USER CRUD OPERATION', () => {
    it('should CREATE a new user and a new wallet', (done) => {
    request(app)
        .post('/testDB/insertUser')
        .send({
        inputName: 'PippoTest',
        inputSurname: 'Pluto',
        inputDateOfBirth: '1996-11-18',
        inputPlaceOfBirth: 'Milano',
        inputAddress: 'Via covid',
        inputCivicNumber: '19',
        inputCity: 'Wuhan',
        inputProvince: 'Hubei',
        inputEmail: 'ciao@ciao.com',
        inputPassword1: 'ciao',
        inputPhoneNumber: '328328328',
        checkSubscription: 'BSC'
        })
        .expect(200, done)
    })
})
//delete
describe('USER CRUD OPERATION', () => {
it('should DELETE user (by name)', (done) => {
    request(app)
    .post('/testDB/deleteUser')
    .send({
        inputName: 'PippoTest',
    })
    .expect(200, done)
})
})
//update
describe('USER CRUD OPERATION', () => {
it('should UPDATE name of a user where name == something', (done) => {
    request(app)
    .post('/testDB/updateUser')
    .send({
        inputName: 'Andrea',
    })
    .expect(200, done)
})
})

//CRUD OPERATION VIDEOGAME
//create
describe('VIDEOGAME CRUD OPERATION', () => {
it('should CREATE a new videogame', (done) => {
    request(app)
    .post('/testDB/insertVideogame')
    .send({
        inputName: 'VideogameTest',
        inputReleaseYear: '2020',
        inputSoftwareHouse: 'Software House Test',
        inputDescription: 'Test Description',
        inputBuyPrice: 1,
        inputRentPrice: 1,
        inputImage: 'ciao',
        inputTrailer: 'ciao'
    })
    .expect(200, done)
})
})
//delete
describe('VIDEOGAME CRUD OPERATION', () => {
it('should DELETE Videogame (by name)', (done) => {
    request(app)
    .post('/testDB/deleteVideogame')
    .expect(200, done)
})
})
//update
describe('VIDEOGAME CRUD OPERATION', () => {
it('should UPDATE name of a Videogame where name == something', (done) => {
    request(app)
    .post('/testDB/updateVideogame')
    .expect(200, done)
})
})

//CRUD OPERATION PLATFORM
//create
describe('PLATFORM CRUD OPERATION', () => {
    it('should CREATE a new platform', (done) => {
        request(app)
        .post('/testDB/insertPlatform')
        .send({
            inputName: 'PlatformTest',
        })
        .expect(200, done)
    })
})
//delete
describe('PLATFORM CRUD OPERATION', () => {
it('should DELETE Platform (by name)', (done) => {
request(app)
.post('/testDB/deletePlatform')
.expect(200, done)
})
})
//update
describe('PLATFORM CRUD OPERATION', () => {
it('should UPDATE name of a Platform where name == something', (done) => {
request(app)
.post('/testDB/updatePlatform')
.expect(200, done)
})
})

//CRUD OPERATION GENRE
//create
describe('GENRE CRUD OPERATION', () => {
    it('should CREATE a new genre', (done) => {
        request(app)
        .post('/testDB/insertGenre')
        .send({
            inputType: 'GenreTest',
        })
        .expect(200, done)
    })
})
//delete
describe('GENRE CRUD OPERATION', () => {
it('should DELETE Genre (by type)', (done) => {
request(app)
.post('/testDB/deleteGenre')
.expect(200, done)
})
})
//update
describe('GENRE CRUD OPERATION', () => {
it('should UPDATE type of a Genre where type == something', (done) => {
request(app)
.post('/testDB/updateGenre')
.expect(200, done)
})
})