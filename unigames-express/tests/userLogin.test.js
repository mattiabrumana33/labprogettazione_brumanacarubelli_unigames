const request = require('supertest')
const app = require('../app_test')

describe('USER LOGIN', () => {
  it('login a user', (done) => {
    request(app)
      .post('/login/auth')
      .send({
        email: 'ciao@ciao.com',
        password: 'ciao',
      })
      .expect(302, done)
  })
})