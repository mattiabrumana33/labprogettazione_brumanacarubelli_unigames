const request = require('supertest')
const app = require('../app_test')

describe('USER LOGOUT', () => {
  it('logout a user', (done) => {
    request(app)
      .post('/login/logout')
    //   .send({
    //     email: 'ciao@ciao.com',
    //     password: 'ciao',
    //   })
      .expect(302, done)
  })
})