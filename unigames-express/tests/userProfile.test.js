const request = require('supertest')
const app = require('../app_test')

describe('USER PROFILE', () => {
    it('update user email and automatically logout ', (done) => {
      request(app)
        .post('/userProfile/updateEmailTEST')
        .send({
            //pass idUser because we implicitly consider that user is logged in, and consider user test
            idUser: '01856446-ea4c-4ce5-befd-708fbeac0b88',
            inputEmail: 'testUpdate@testUpdate.com'
        })
        .expect(200, done)
    })
  })


describe('USER PROFILE', () => {
    it('update user password and automatically logout ', (done) => {
      request(app)
        .post('/userProfile/updatePasswordTEST')
        .send({
            //pass idUser because we implicitly consider that user is logged in, and consider user test
            idUser: '01856446-ea4c-4ce5-befd-708fbeac0b88',
            inputPassword1: 'testUpdate'
        })
        .expect(302, done)
    })
})

describe('USER PROFILE', () => {
    it('update user which is logged in', (done) => {
      request(app)
        .post('/userProfile/updateUserTEST')
        .send({
            //pass idUser because we implicitly consider that user is logged in, and consider user test
            idUser: '01856446-ea4c-4ce5-befd-708fbeac0b88',
            inputName: 'TESTUPDATE',
            inputSurname: 'Pluto',
            inputDateOfBirth: '1996-11-18',
            inputPlaceOfBirth: 'Milano',
            inputAddress: 'Via covid',
            inputCivicNumber: '19',
            inputCity: 'Wuhan',
            inputProvince: 'Hubei',
            inputPhoneNumber: '328328328',
        })
        .expect(200, done)
    })
})