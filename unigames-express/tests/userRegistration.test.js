const request = require('supertest')
const app = require('../app_test')

// describe('Sample Test', () => {
//     it('should test that true === true', () => {
//       expect(true).toBe(true)
//     })
//   })

// it('Gets videogames', async done => {
//     const response = await request.get('/videogames')

//     expect(response.status).toBe(200)
//     //expect(response.body.message).toBe('pass!')
//     done()
//   })

describe('USER POST', () => {
  it('should create a new user', (done) => {
    request(app)
      .post('/userRegistration/user')
      .send({
        // idUser: 'dab5b676-45a1-4cb7-bb53-26a9ff7787aa',
        // creationDate: '2020-03-30 18:07:08',
        inputName: 'Pippo',
        inputSurname: 'Pluto',
        inputDateOfBirth: '1996-11-18',
        inputPlaceOfBirth: 'Milano',
        inputAddress: 'Via covid',
        inputCivicNumber: '19',
        inputCity: 'Wuhan',
        inputProvince: 'Hubei',
        inputEmail: 'ciao@ciao.com',
        inputPassword1: 'ciao',
        inputPhoneNumber: '328328328',
        // admin: 0,
        checkSubscription: 'BSC'
        // idWallet: '448ad859-ea7e-45b9-8cb5-c8ecef0125f8',
        // realBalance: 0,
        // virtualBalance: 0,
      })
      .expect(302, done)
  })
})