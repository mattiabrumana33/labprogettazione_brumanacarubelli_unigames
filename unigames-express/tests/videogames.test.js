const request = require('supertest')
const app = require('../app_test')

//New Videogame GET
describe('NEW VIDEOGAME GET', () => {
    it('should test that true = true', () => {
        expect(true).toBe(true)
    })
})

//New Videogame POST
describe('NEW VIDEOGAME POST', () => {
    it('should create a new videogame', (done) => {
        request(app)
            .post('/videogames/newVideogame')
            .send({
                inputTitle: '_Test_Videogame',
                inputReleaseYear: '2014',
                inputSoftwareHouse: 'Unigames Studios',
                inputDescription: 'An Assassin game.',
                inputBuyPrice: '19.99',
                inputRentPrice: '5.99',
                inputPlatforms: ['6','7','8'],
                inputGenres: ['3','4','5']
            })
            .expect(302, done)
    })
})

//Edit Videogame POST
describe('Edit Videogame POST', () => {
    it('should modify an existing videogame', (done) => {
        request(app)
            .post('/videogames/editVideogame')
            .send({
                inputId: '11',
                inputTitle: '__TestVideogame',
                inputReleaseYear: '2012',
                inputSoftwareHouse: 'Unigames Studiosi',
                inputDescription: 'An Assassin game very beautiful.',
                inputBuyPrice: '9.99',
                inputRentPrice: '2.99',
                inputPlatforms: ['9','10','11'],
                inputGenres: ['6','7','8']
            })
            .expect(302, done)
    })
})

//Remove Genre GET
describe('Remove Genre GET', () => {
    it('should remove an existing videogame', (done) => {
        request(app)
            .get('/videogames/removeVideogame/12')
            .expect(302, done)
    })
})
